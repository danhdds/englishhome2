json.(user, *User.column_names)
json.mugshot_img_url  user.mugshot.url(:original) if user.mugshot_file_name.present?
