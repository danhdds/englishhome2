json.files [@import] do |json, import|
  json.name                   import.upload_file_name
  json.size                   import.upload_file_size
  json.url                    import.upload.url(:original)
  json.delete_url             admin_import_path(import)
  json.delete_type            "DELETE"
end