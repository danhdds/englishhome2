json.files [@design] do |json, design|
  json.name                   design.list_photo_file_name
  json.size                   design.list_photo_file_size
  json.url                    design.list_photo.url(:original)
  json.delete_url             admin_design_path(design)
  json.delete_type            "DELETE"
end
