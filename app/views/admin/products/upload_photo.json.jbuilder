json.files [@product] do |json, product|
  json.name                   product.photo_file_name
  json.size                   product.photo_file_size
  json.url                    product.photo.url(:original)
  json.delete_url             admin_design_path(product)
  json.delete_type            "DELETE"
end