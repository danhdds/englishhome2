json.code                             product.code
json.base_colour_id                   product.base_colour_id
json.base_fabric_id                   product.base_fabric_id
json.design_id                        product.design_id
json.product_code_id                  product.product_code_id
json.shape_id                         product.shape_id
json.applique1_id                     product.applique1_id
json.applique2_id                     product.applique2_id
json.applique3_id                     product.applique3_id
json.name                             product.name
json.with_option_id                   product.with_option_id
json.and_option_id                    product.and_option_id
json.dimension_id                     product.dimension_id
json.style_id                         product.style_id
json.photo_file_name                  product.photo_file_name
json.photo_content_type               product.photo_content_type
json.photo_file_size                  product.photo_file_size
json.other1_id                        product.other1_id
json.other2_id                        product.other2_id
json.price                            product.price
json.delta                            product.delta
json.photo_styles                     product.photo_styles
json.short_code                       product.short_code
json.position                         product.position
json.created_at                       product.created_at
json.updated_at                       product.updated_at
json.short_name                       product.short_name

json.base_colour                      product.base_colour
json.base_fabric                      product.base_fabric
json.design                           product.design
json.product_code                     product.product_code
json.shape                            product.shape
json.applique1                        product.applique1
json.applique2                        product.applique2
json.applique3                        product.applique3
json.with_option                      product.with_option
json.and_option                       product.and_option
json.dimension                        product.dimension
json.style                            product.style
json.other1                           product.other1
json.other2                           product.other2
json.preview_img_url                  product.photo.url(:preview) if product.photo_file_name.present?
json.formatted_price                  product.price ? number_to_currency(product.price, :unit => "£") : "Unpriced"
json.enabled                          product.enabled
json.dominant_colour                   product.dominant_colour || "#dddddd"

json.audits product.audits do |json, audit|
  json.action       audit.action.capitalize
  json.user_name    audit.user.presence ? audit.user.full_name : "Unknown User"
  json.changes      audit.audited_changes
  json.created_at   audit.created_at.to_formatted_s(:short_and_year)
end
