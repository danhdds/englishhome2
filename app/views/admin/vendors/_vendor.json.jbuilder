json.name             vendor.name
json.position         vendor.position
json.map_url          vendor.map_url
json.address          RedCloth.new(vendor.address)
json.head_office      vendor.head_office
json.show             vendor.show
json.google_map_img   vendor.google_map_img