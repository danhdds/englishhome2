json.text             RedCloth.new(site_text.text)
json.enabled          site_text.enabled
json.about_us         site_text.about_us
json.trade            site_text.trade
json.shipping         site_text.shipping
