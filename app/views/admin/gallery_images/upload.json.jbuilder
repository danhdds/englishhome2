json.files [@gallery_image] do |json, gallery_image|
  json.name                   gallery_image.image_file_name
  json.size                   0
  json.url                    gallery_image.image.url(:original)
  json.delete_url             admin_gallery_image_path(gallery_image)
  json.delete_type            "DELETE"
end