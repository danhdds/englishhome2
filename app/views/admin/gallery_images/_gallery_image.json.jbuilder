# json.attributes do |json|
  json.(gallery_image, *GalleryImage.column_names)
  json.preview_img_url  gallery_image.image.url(:zoom) if gallery_image.image_file_name.present?
  json.admin_img_url  gallery_image.image.url(:admin) if gallery_image.image_file_name.present?

  json.collection1      gallery_image.collection1
  json.collection2      gallery_image.collection2
  json.collection3      gallery_image.collection3
# end

