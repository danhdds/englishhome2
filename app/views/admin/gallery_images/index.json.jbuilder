json.array! @gallery_images do |json, gallery_image|
  json.partial! "admin/gallery_images/gallery_image", :gallery_image => gallery_image
end