json.id                          @menu_image.id
json.products_classic            @menu_image.products_classic
json.products_contemporary       @menu_image.products_contemporary
json.collections_classic         @menu_image.collections_classic
json.collections_contemporary    @menu_image.collections_contemporary
json.enabled                     @menu_image.enabled
json.preview_img_url             @menu_image.photo.url(:original) if @menu_image.photo.exists?
