json.files [@menu_image] do |json, menu_image|
  json.name                   menu_image.photo_file_name
  json.size                   menu_image.photo_file_size
  json.url                    menu_image.photo.url(:original)
  json.delete_url             admin_menu_image_path(menu_image)
  json.delete_type            "DELETE"
end
