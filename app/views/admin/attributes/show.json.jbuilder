json.code                   @attribute.code
json.name                   @attribute.name || "Unnamed"
json.type                   @attribute.type
json.colour                 @attribute.colour
json.detail                 @attribute.detail
json.metric_description     @attribute.metric_description
json.imperial_description   @attribute.imperial_description
json.description            @attribute.description
json.dimension              @attribute.dimension
json.plural                 @attribute.plural
json.present_in_last_import @attribute.present_in_last_import
json.photo_img_url          @attribute.photo.url(:preview) if @attribute.photo_file_name.present?
json.list_photo_img_url     @attribute.list_photo.url(:preview) if @attribute.list_photo_file_name.present?
