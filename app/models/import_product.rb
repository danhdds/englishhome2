class ImportProduct < ActiveRecord::Base
  attr_accessible :product, :updated, :import
  belongs_to :import
  belongs_to :product

  scope :updated, :conditions => {:updated => true}

end
