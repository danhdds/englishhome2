class User < ActiveRecord::Base
  acts_as_authentic
  # has_many :changes, :foreign_key => :whodunnit, :class_name => "Version", :order => "created_at DESC"

  attr_accessible :email, :admin, :password, :password_confirmation, :first_name, :last_name,
                  :bio, :position, :mugshot_file_name


  acts_as_list
  has_attached_file :mugshot,
      :styles => {
        :admin=> "48x48#",
        :thumb => "150x" },
      :convert_options => {
        :thumb => "-colorspace RGB -strip ",
        :zoom => "-colorspace RGB -strip ",
      },
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename"

  def full_name
    "#{first_name} #{last_name}"
  end



  def short_name
    "#{first_name} #{last_name[0,1]}"
  end

  scope :with_bios, :conditions => "bio IS NOT NULL and mugshot_file_name IS NOT NULL", :order => "position ASC"

end
