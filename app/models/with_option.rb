class WithOption < Attribute
  # has_many :products

  def products
    Product.has_option(to_param)
  end
  # def product_count
  #   Product.count(:conditions => ["with_option_id = ? OR and_option_id = ?", id, id])
  # end

  class << self
    def mask
      /^(.{4})(.*)/
    end

    def length
      4
    end

  end
end
