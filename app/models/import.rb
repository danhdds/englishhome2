class Import < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :upload, :import_type, :user
  has_attached_file :upload

  include Rails.application.routes.url_helpers
  def to_jq_upload
    {
      "name" => read_attribute(:upload_file_name),
      "size" => read_attribute(:upload_file_size),
      "url" => upload.url(:original),
      "delete_url" => admin_import_path(self),
      "delete_type" => "DELETE"
    }
  end

  PRODUCTS = "products"
  PRICES = "prices"
  SHIPPING = "shipping"
  PEOPLE = "people"

  belongs_to :user
  has_many :import_products
  has_many :products, :through => :import_products

  has_many :created, :through => :import_products, :class_name => "Product", :conditions => {"import_products.updated" => false}, :source => :product
  has_many :updated, :through => :import_products, :class_name => "Product", :conditions => {"import_products.updated" => true}, :source => :product
  self.inheritance_column = :import_type
end
