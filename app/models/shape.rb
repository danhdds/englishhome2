class Shape < Attribute
  has_many :products

  class << self
    def mask
      /^([A-Z]{3})(.*)/
    end

    def length
      3
    end

  end
end
