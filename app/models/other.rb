class Other < Attribute

  def products
    Product.has_other(to_param)
  end


  class << self

    def import_from_row(e, line)
      code =   e.cell(line, "A")
      name =   [e.cell(line, "B"), e.cell(line, "C"), e.cell(line, "D")].compact.join(", ")


      {:code => code, :name => name }
    end

    def mask
      /^([A-Z]{5})(.*)/
    end

    def length
      5
    end

  end
end
