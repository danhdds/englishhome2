class SiteText < ActiveRecord::Base
  attr_accessible :about_us, :enabled, :text, :trade, :shipping

  scope :enabled, where(:enabled => true)
  scope :about_us, where(:about_us => true)
  scope :trade, where(:trade => true)
  scope :shipping, where(:shipping => true)
  scope :limited, limit(1)

  before_save :allow_only_one_text_per_location

  def allow_only_one_text_per_location
    if self.about_us? && self.enabled?
      SiteText.where(:about_us => true, :enabled => true).
        update_all(:enabled => false)
    end

    if self.trade? && self.enabled?
      SiteText.where(:trade => true, :enabled => true).
        update_all(:enabled => false)
    end

    if self.shipping? && self.enabled?
      SiteText.where(:shipping => true, :enabled => true).
        update_all(:enabled => false)
    end
  end
end
