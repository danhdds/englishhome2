class Attribute < ActiveRecord::Base
  attr_accessible :code, :name, :description, :dimension, :column_count,
                  :hide_from_countries, :plural, :present_in_last_import,
                  :collection_type, :design_position, :photo, :list_photo
  has_many :products
  validates_presence_of :code

  default_scope order('attributes.name ASC')

  def code=(value)
    write_attribute(:code, value.strip)
  end

  def product_count
    products.count
  end

  def value_type= value_type
    logger.info("Set type #{value_type}")
    self.type = value_type
  end

  def value_type
    self.type
  end

  def human_name
    name
  end

  def to_s
    name
  end

  def mask
    /^(#{code})/i
  end



  class << self
    def inherited(child)
      child.instance_eval do
        def model_name
          Attribute.model_name
        end
      end
      super
    end

    def import_from_excel(excel_document)
      Rails.logger.info "IMPORTING #{self.name}".red
      Rails.logger.info "ROWS: #{excel_document.last_row}"
      Rails.logger.info "#################################"
      2.upto(excel_document.last_row) do |line|
        attributes = import_from_row(excel_document, line)

        if attributes && attributes[:code].present?
          record = find_or_initialize_by_code(attributes)
          if record.new_record?
            record.save
          else
            record.update_attributes(attributes.merge(:present_in_last_import => true))
          end
        end
      end
    end

    def import_from_row(e, line)
      code =   read_code_from_columm(e, line, "A")
      name =   e.cell(line, "B")

      {:code => code, :name => name}
    end

    def read_code_from_columm(e, line, column)
      code = e.cell(line, column)
      case e.celltype(line, column)
        when :float
          code.to_i.to_s.strip
        when :string
          code
      end
    end

    def prefix
      ""
    end

    def mask
      /^(\w)(.*)/
    end

    def select_options
      find(:all, :order=>"code").collect {|f| ["#{f.code}, #{f.name}", f.id]}
    end


    def match(code)
      if code=~mask
      end
    end

    def blank_mask
      /^(#{"O"*length})(.*)/
    end
    def decode_code(code)
      puts name
      puts code
      if code=~blank_mask
        return nil, $2
      elsif code=~mask

        result = attribute_code = nil
        self.unscoped.order("LENGTH(code) DESC").each { |e|
          puts e.mask
          if code =~ e.mask
            puts "found: #{e.mask} matching with #{code}"
            attribute_code = $1
            result = e
            break
          end
        }
        puts attribute_code if attribute_code.present?
        # puts code[attribute_code.length, code.length]
        return result, attribute_code.present? ? code[attribute_code.length, code.length] : code
      else
        return nil, code
      end
    end
  end

  # def self.model_name
  #   name = "attribute"
  #   name.instance_eval do
  #     def plural;   pluralize;   end
  #     def singular; singularize; end
  #     def singular_route_key; singularize; end
  #   end
  #   return name
  # end


end
