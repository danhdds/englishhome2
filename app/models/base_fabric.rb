class BaseFabric < Attribute
  has_many :products

  class << self

    def import_from_row(e, line)
      code =   read_code_from_columm(e, line, "A")
      name =   e.cell(line, "B")
      description = e.cell(line, "C")

      attributes = {:code => code, :name => name, :description => description}
    end

    def mask
      /^(\d{2})/
    end

    def length
      2
    end

  end
end
