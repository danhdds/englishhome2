class ProductCode < Attribute
  acts_as_list :column => "product_code_position"
  has_many :products
  has_many :product_shipping_codes
  # named_scope :active, :select => "attributes.*, (select count(products.id) from products where products.product_code_id = attributes.id) as product_count", :conditions => "product_count>0"
  def self.active
    ProductCode.all(:joins => "INNER JOIN products ON products.product_code_id = attributes.id", :select => "attributes.*, count(products.id) as product_count", :group => "attributes.id", :order => "products.name ASC")
  end

  def self.classic_active
    ProductCode.includes(:products).
      where("products.collection_type = ? AND
        products.photo_file_name IS NOT NULL AND
        products.price IS NOT NULL", true).
      order("products.name ASC")
  end

  def self.contemporary_active
    ProductCode.includes(:products).
      where("products.collection_type = ? AND
        products.photo_file_name IS NOT NULL AND
        products.price IS NOT NULL", false).
      order("products.name ASC")
  end

  def to_param
    "#{id}"
  end

  def shipping_info(country)
    self.product_shipping_codes.all(:order => "quantity ASC").inject([]) do |array, psc|
      shipping_code = psc.shipping_code
      shipping_code_country = shipping_code.shipping_code_countries.select { |scc| scc.country == country }.first
      array << {:quantity => psc.quantity, :code => shipping_code.code, :cost =>shipping_code_country.cost.to_f} if shipping_code_country.present?

      array
    end
  end

  def hidden_from_countries
    self.hide_from_countries.present? ? self.hide_from_countries.split(/,|\s/).delete_if {|x| x.blank?} : []
  end

  class << self

    def import_from_row(e, line)
      code =   read_code_from_columm(e, line, "A")
      name =   e.cell(line, "B")
      description =   e.cell(line, "C")
      dimension =   e.cell(line, "E") || "160"
      column_count = e.cell(line, "F") || 5
      hide_from_countries = e.cell(line, "G")
      plural = e.cell(line, "H")

      {:code => code,
        :name => name,
        :description => description,
        :dimension => dimension,
        :column_count => column_count,
        :hide_from_countries => hide_from_countries,
        :plural => plural,
        :product_code_position => line-1}
    end

    def mask
      /^([A-Z]{3})(.*)/
    end

    def length
      3
    end
  end

end
