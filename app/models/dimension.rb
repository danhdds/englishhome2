class Dimension < Attribute
  has_many :products

  class << self

    def import_from_row(e, line)
      code =   read_code_from_columm(e, line, "A")
      name = [e.cell(line, "B"), e.cell(line, "C")].compact.join(" / ")
      metric_description =   e.cell(line, "B")
      imperial_description =   e.cell(line, "C")

      {:code => code, :name => name }
    end

    def mask
      /^([A-Z0-9]{7})(.*)/i
    end

    def length
      7
    end


    def select_options
      find(:all, :order=>"name").collect {|f| ["#{f.code} - #{f.name}", f.id]}
    end
  end
end
