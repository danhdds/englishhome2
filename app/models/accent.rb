class Accent < Attribute
  has_many :products

  class << self
    def mask
      /^(\d{1})(.*)/
    end

    def length
      1
    end
  end

end
