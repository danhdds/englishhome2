class Product < ActiveRecord::Base
  attr_accessible :code, :price, :short_code, :price, :position, :enabled, :photo
  audited
  FACET_ATTRIBUTES = [:design_id, :base_colour_id, :base_fabric_id, :product_code_id]

  has_attached_file :photo,
      :styles => {
        :admin=> "48x48",
        :related => "120x120",
        :thumb => "160x160",
        :preview  => "400x700",
        :zoom => "800x1400"
      },
      :default_url => "/assets/logo.png",
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename",
      :convert_options => {
        :zoom => "-quality 80 -strip -gravity center -extent 900 -background white",
        :admin => "-quality 80 -gravity center -extent 48x48",
        :thumb => "-colorspace sRGB -strip -quality 85",
        :preview => "-quality 80 -gravity center -extent 400 -background white"
      }

  after_post_process :get_image_colour
  before_save :set_collection_type

  scope :contemporary, where(:collection_type => false)
  scope :classic, where(:collection_type => true)

  def set_collection_type
    self.collection_type = design.collection_type == "Classic" if design.present?
    true
  end

  def get_image_colour
    Rails.logger.info photo.queued_for_write[:original].inspect
    img = Magick::Image::read(photo.queued_for_write[:original].path).first
    total = 0
    avg = { :r => 0.0, :g => 0.0, :b => 0.0 }

    img.quantize.color_histogram.each do |c,n|
      avg[:r] += n * c.red
      avg[:g] += n * c.green
      avg[:b] += n * c.blue
      total += n
    end

    avg.each_key do |c|
      avg[c] /= total
      avg[c] = (avg[c] / Magick::QuantumRange * 255).to_i
    end

    self.dominant_colour = "rgb(#{avg[:r]},#{avg[:g]},#{avg[:b]})"
  end

  belongs_to :product_code, :counter_cache => true
  belongs_to :base_fabric, :counter_cache => true
  belongs_to :design, :counter_cache => true
  belongs_to :shape, :counter_cache => true
  belongs_to :base_colour, :counter_cache => true

  scope :has_applique, lambda { |pid| { :conditions=>["applique1_id = :applique_id OR applique1_id = :applique_id OR applique1_id = :applique_id",  { :applique_id => pid } ] } }
  belongs_to :applique1, :class_name => "Applique", :counter_cache => true
  belongs_to :applique2, :class_name => "Applique", :counter_cache => true
  belongs_to :applique3, :class_name => "Applique", :counter_cache => true

  scope :has_option, lambda { |pid| { :conditions=>["with_option_id = :option_id OR  and_option_id = :option_id",  { :option_id => pid } ] } }
  belongs_to :with_option, :class_name => "WithOption", :counter_cache => true
  belongs_to :and_option, :class_name => "WithOption", :counter_cache => true


  scope :has_other, lambda { |pid| { :conditions=>["other1_id = :other_id OR other2_id = :other_id",  { :other_id => pid } ] } }
  belongs_to :other1, :class_name => "Other", :counter_cache => true
  belongs_to :other2, :class_name => "Other", :counter_cache => true

  belongs_to :dimension, :counter_cache => true
  belongs_to :product_style, :counter_cache => true
  belongs_to :accent, :counter_cache => true

  default_scope :order => "position DESC"
  scope :no_short_code, :conditions => ["short_code IS NULL OR short_code = ''"]
  scope :no_price, :conditions => ["price IS NULL OR price = 0"]
  has_many :import_products

  scope :no_photo, where("photo_file_name IS NULL")
  scope :has_photo, where("photo_file_name IS NOT NULL")
  scope :enabled,  where("enabled IS true")



  ATTRIBUTES = {
    :product_code => ProductCode,
    :base_fabric => BaseFabric,
    :design => Design,
    :shape => Shape,
    :base_colour => BaseColour,
    :applique1 => Applique,
    :applique2 => Applique,
    :applique3 => Applique,
    :with_option => WithOption,
    :and_option => WithOption,
    :other1 => Other,
    :other2 => Other,
    :dimension => Dimension
  }

  ATTRIBUTE_NAMES = ATTRIBUTES.keys

  def related_products
    Product.where(:design_id => design_id).
      where("(NOT id = ?) AND
        (photo_file_name IS NOT NULL) AND (price IS NOT NULL)", id)
  end

  def thumbnail_dimension
    product_code.present? ? product_code.dimension : "160"
  end

  def thumbnail_extent
    product_code.present? ? product_code.extent : "160x160"
  end


  def style
    Paperclip::Style.new(:custom, {:geometry => thumbnail_dimension}, photo)
  end

  def convert_options
    "-quality 70"
  end

  def subtitle
    result = ""
    if shape.present?
      result += "#{shape.name} "
    elsif product_code.code=="E"
      result += "Square "
    end
    result += "#{product_code.name} " if product_code.present?
  end

  def short_name
    result = ""
    result += "#{design.name} " if design.present?
    result += "#{shape.name} " if shape.present?
    result += "#{base_fabric.name} " if base_fabric.present?
    result.strip!
    result.present? ? result : name
  end

  def to_param
    "#{id}-#{name.strip.gsub(/[^a-z0-9]+/i, '-')}"
  end

  def name
    self[:name] || "Unnamed Product"
  end

  #
  # A,B,C,D,E
  def interpret_code
    # if self.code_changed?

    #   puts

    #   code ||= self.code.upcase
    #   depth ||= 0
    #   indent = " " * depth


    #   attribute ||= ATTRIBUTES.keys.first
    #   next_attribute = ATTRIBUTE_NAMES[ATTRIBUTE_NAMES.index(attribute)+1]

    #   puts "#{indent} #{code}, #{attribute}, #{next_attribute}"
    #   if code =~ ATTRIBUTES[attribute].mask
    #     match = if next_attribute
    #       puts "#{indent} Searching all #{attribute} for #{code}"
    #       matching_attributes = ATTRIBUTES[attribute].unscoped.order("LENGTH(code) DESC").select do |a|
    #         # puts "#{attribute}: #{a.code} #{a.mask}"
    #         code =~ a.mask
    #       end

    #       puts "#{indent} Matches found: #{matching_attributes.map(&:code).join(",")}"
    #       matching_attributes.detect do |a|

    #         m = code.match(a.mask)
    #         attribute_code = m[1]

    #         remainder_code = code[attribute_code.length, code.length]
    #         puts "#{indent} Found a match for #{attribute}: #{a.code} #{attribute_code}, remainder_code: #{remainder_code}"
    #         interpret_code(remainder_code, next_attribute, depth+1)
    #       end

    #     else
    #       ATTRIBUTES[attribute].unscoped.order("LENGTH(code) DESC").detect do |a|
    #         code =~ a.mask
    #       end
    #     end

    #     if match
    #       self.enabled = true
    #       send("#{attribute.to_s}=", match)
    #       true
    #     else
    #       puts "#{indent} A: Cannot find a #{attribute} for #{code}, so skipping: #{next_attribute.present?} "
    #       interpret_code(code, next_attribute, depth+1) if next_attribute.present?

    #     end
    #   else
    #     puts "#{indent} B: Cannot find a #{attribute} for #{code}, so skipping: #{next_attribute.present?}"
    #     interpret_code(code, next_attribute, depth+1) if next_attribute.present?
    #   end

      tmp_code = code.upcase
      puts tmp_code



    # #   attributes.each do |a|
    # #     match_attribute(a, )
    # #   end

      self.product_code, tmp_code = ProductCode.decode_code(tmp_code)
      self.base_fabric, tmp_code = BaseFabric.decode_code(tmp_code)
      self.design, tmp_code = Design.decode_code(tmp_code)
      self.shape, tmp_code = Shape.decode_code(tmp_code)
      self.base_colour, tmp_code = BaseColour.decode_code(tmp_code)

      self.applique1, tmp_code = Applique.decode_code(tmp_code)
      self.applique2, tmp_code = Applique.decode_code(tmp_code)
      self.applique3, tmp_code = Applique.decode_code(tmp_code)

      self.with_option, tmp_code = WithOption.decode_code(tmp_code)
      self.and_option, tmp_code = WithOption.decode_code(tmp_code)

      self.other1, tmp_code = Other.decode_code(tmp_code)
      self.other2, tmp_code = Other.decode_code(tmp_code)

      self.dimension, tmp_code = Dimension.decode_code(tmp_code)
      self.accent, tmp_code = Accent.decode_code(tmp_code)
      # self.product_style, tmp_code = ProductStyle.decode_code(tmp_code)
      if tmp_code.length>0
        disable
        Rails.logger.info "Disabled - #{code}, #{tmp_code}, #{id}"
      end
      self.name = get_name
    # # end
    # self
  end

  def disable
    self.enabled = false
  end

  def get_name
    result = ""
    result += "#{design.name} " if design.present?
    result += "#{base_colour.name} " if base_colour.present?
    result += "#{shape.name} " if shape.present?
    result += "#{base_fabric.name} " if base_fabric.present?
    result += "#{product_code.name} " if product_code.present?

    #
    # Product.reflections.values.select {|e| ATTRIBUTE_TYPES.include?(e.klass.to_s)}.inject("") { |result,e|
    #   property = self.send(e.name.to_sym)
    #
    #   property.present? ? result+="#{property.name} " : result
    # }.strip
  end

  class << self

    def process_facets(facets)
      facets.inject({}) do |result, (k,v)|
        if k.to_s.match /_id$/
          attribute_class = k.to_s.gsub(/_id$/, "").classify.constantize
          result[k] = v.inject({}) {|r, (id, count)|
            attribute = attribute_class.find_by_id(id)
            r[attribute] = count if attribute
            r
          }
        end
        result
      end
    end

    def per_page
      18
    end
  end
end
