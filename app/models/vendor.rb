class Vendor < ActiveRecord::Base
  attr_accessible :address, :map_url, :name, :position, :head_office, :show, :google_map_img

  before_save :check_head_office

  scope :non_head_office, where(:head_office => false)
  scope :and_to_be_shown, where(:show => true)

  def self.head_office
    Vendor.find_by_head_office(true)
  end

  # Only allow one vendor to be the head office.
  def check_head_office
    return unless self.head_office == true
    if head_office_vendor = Vendor.head_office
      head_office_vendor.update_attribute(:head_office, false)
    end
  end

end
