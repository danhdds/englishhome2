class MenuImage < ActiveRecord::Base
  attr_accessible :enabled, :collections_classic, :collections_contemporary,
                  :products_classic, :products_contemporary, :photo

  scope :enabled, where(:enabled => true)
  scope :collections_classic, where(:collections_classic => true)
  scope :collections_contemporary, where(:collections_contemporary => true)
  scope :products_classic, where(:products_classic => true)
  scope :products_contemporary, where(:products_contemporary => true)
  scope :limited, limit(1)

  has_attached_file :photo,
      :styles => {
        :admin => "48x48",
        :menu  => "448x200"
      },
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename"

  before_save :allow_only_one_text_per_location

  def allow_only_one_text_per_location
    if self.collections_classic? && self.enabled?
      MenuImage.where(:collections_classic => true, :enabled => true).
        update_all(:enabled => false)
    end

    if self.collections_contemporary? && self.enabled?
      MenuImage.where(:collections_contemporary => true, :enabled => true).
        update_all(:enabled => false)
    end

    if self.products_classic? && self.enabled?
      MenuImage.where(:products_classic => true, :enabled => true).
        update_all(:enabled => false)
    end

    if self.products_contemporary? && self.enabled?
      MenuImage.where(:products_contemporary => true, :enabled => true).
        update_all(:enabled => false)
    end
  end
end
