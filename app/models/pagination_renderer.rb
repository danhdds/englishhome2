class PaginationRenderer < WillPaginate::ActionView::LinkRenderer

  # def to_html
  #   links = @options[:page_links] ? windowed_links : []
  #   # previous/next buttons
  #   links.unshift page_link_or_span(@collection.previous_page, 'disabled prev_page', @options[:previous_label])
  #   links.push    page_link_or_span(@collection.next_page,     'disabled next_page', @options[:next_label])

  #   links.push          @template.link_to "View All", @template.url_for(:view_all=>1)

  #   html = links.join(@options[:separator])
  #   html = html.html_safe if html.respond_to? :html_safe
  #   @options[:container] ? @template.content_tag(:div, html, html_attributes) : html

  # end

  protected

    def page_number(page)
      unless page == current_page
        link(page, page, :rel => rel_value(page))
      else
        tag(:span, page, :class => "current")
      end
    end

    def previous_or_next_page(page, text, classname)
      if page
        link(text, page, :class => classname)
      else
        tag(:span, text, :class => classname + ' disabled')
      end
    end

    def html_container(html)
      tag(:div, html, container_attributes)
    end


end