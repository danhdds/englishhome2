class Design < Attribute
  has_many :products

  def self.active(limit = nil)
    Design.all(:joins => "INNER JOIN products ON products.design_id = attributes.id", :select => "attributes.*, count(products.id) as product_count", :group => "attributes.id", :order => "design_position ASC, name ASC", :limit => limit)
  end

  scope :contemporary, where(:collection_type => "Contemporary")
  scope :classic, where(:collection_type => "Classic")

  scope :has_photo, where('attributes.photo_file_size > 0')
  scope :has_list_photo, where('attributes.list_photo_file_size > 0')

  has_attached_file :photo,
      :styles => {
        :admin=> "48x48",
        :related => "120x120",
        :thumb => "160x160",
        :preview  => "400x700",
        :moodshot => "960",
        :zoom => "800x1400"
      },
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename",
      :convert_options => {
        :preview => "-strip -gaussian-blur 0.05 -quality 85 -extent 400",
        :moodshot => "-strip -gaussian-blur 0.05 -quality 85 -extent 960"
      }

  has_attached_file :list_photo,
      :styles => {
        :admin=> "48x48",
        :preview  => "320x320"
      },
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename"

  def mask
    /^(#{code})/
  end

  # def to_param
  #   "#{id}#{name.present? ? "-"+name.strip.gsub(/[^a-z0-9]+/i, '-') : ""}"
  # end

  class << self

    def mask
      /^(\d{3})(.*)/
    end

    def length
      3
    end

    def import_from_row(e, line)
      Rails.logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^".cyan
      code =            read_code_from_columm(e, line, "A")
      name =            e.cell(line, "B")
      contemporary  =   e.cell(line, "C") == "Y"

      collection_type = contemporary ? "Contemporary" : "Classic"
      Rails.logger.info "Collection Type #{collection_type.red}".cyan

      {:code => code, :name => name, :design_position => line-1, :collection_type => collection_type}
    end

  end
end
