class GalleryImage < ActiveRecord::Base
  acts_as_list
  has_attached_file :image,
      :styles => {
        :admin=> "48x48",
        :thumb => "144x96^",
        :zoom => "800x531^" },
      :convert_options => {
        :thumb => "-colorspace RGB -strip ",
        :zoom => "-colorspace RGB -strip ",
      },
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename"


  belongs_to :collection1, :class_name => "Design"
  belongs_to :collection2, :class_name => "Design"
  belongs_to :collection3, :class_name => "Design"
  attr_accessible :home_page, :image_file_name, :postiion, :collection1_id, :collection2_id, :collection3_id, :image

  scope :to_be_shown, where(:home_page => true)
end