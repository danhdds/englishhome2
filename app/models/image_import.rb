class ImageImport < Import
  after_save :process_upload

  def to_jq_upload
    {
      "name" => read_attribute(:upload_file_name),
      "size" => read_attribute(:upload_file_size),
      "url" => upload.url(:original),
      "delete_url" => admin_import_path(self),
      "delete_type" => "DELETE",
      "product" => imported_product

    }
  end

  def imported_product
    import_products.first.product
  end

  def process_upload
    Rails.logger.info upload
    Rails.logger.info upload.path
    code = File.basename(upload.path, '.*')

    product = Product.find_by_code(code) || Product.new(:code => code)
    product.interpret_code
    product.photo = File.open(upload.path)

    product_is_new = product.new_record?
    product.save!
    if product_is_new
      self.import_products.create(:product => product, :updated => false)
    else
      self.import_products.create(:product => product, :updated => true)
    end
  end


end