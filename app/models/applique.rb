class Applique < Attribute
  def products
    Product.has_applique(to_param)
  end

  def mask
    /^(#{code})/
  end

  def to_s
    "#{self.code} - #{[self.name, self.colour, self.detail].compact.join(", ")}"
  end

  class << self

    def import_from_row(e, line)
      code =   read_code_from_columm(e, line, "A").upcase
      if code =~ mask

        code = $1
        name =   e.cell(line, "B")
        description = [e.cell(line, "C"), e.cell(line, "D")].compact.join(", ")

        {:code => code, :name => name, :description => description}
      end
    end

    def length
      5
    end

    def mask
      /^(.{5})(.*)/
    end

    def select_options
      find(:all, :order=>"name").collect {|f| ["#{f.code} - #{[f.name, f.colour, f.detail].compact.join(", ")}", f.id]}
    end
  end
end
