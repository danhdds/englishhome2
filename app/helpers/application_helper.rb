module ApplicationHelper
  def sortable_th(options = nil, &block)
    default_options = { :sort_direction => "desc",
                        :sort_key => "created_at"}
    options = default_options.merge(options)
    url_options = { :sort_direction => options[:sort_direction],
                    :sort_key => options[:sort_key] }
    is_sort_column = @sort_key == options[:sort_key]
    url_options[:sort_direction] = inverse_sort_direction(options[:sort_direction]) if is_sort_column


    url = url_for(request.GET.merge(url_options))
    content_tag(:th, {:class => "sortable #{options[:sort_direction]} #{options[:class]} #{"sorted" if is_sort_column}", "sort-url" => url}) do
      text = capture(&block).html_safe
      text.safe_concat tag("span", :class => "arrow #{options[:sort_direction]}") if is_sort_column
      text
    end.html_safe
  end

  def inverse_sort_direction(sort_direction)
    case sort_direction.to_sym
    when :asc
      "desc"
    when :desc
      "asc"
    else
      "desc"
    end
  end
end
