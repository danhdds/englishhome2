module CategoriesHelper
  FILTER_MAP = {:design_id => "Collection",
                :base_fabric_id => "Fabric",
                :base_colour_id => "Colour"}
  def filter_name(key)
    "Filter by #{FILTER_MAP[key] || key.to_s.titleize}"
  end
end
