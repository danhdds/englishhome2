class ErrorsController < ApplicationController

  before_filter :load_menu

  def not_found
    Rails.logger.info "\n\n----> In not found"
    @title = "404"
    respond_to do |format|
      Rails.logger.info "\n\n----> About to render"
      format.html { render template: 'errors/not_found', layout: 'layouts/shop', status: 404 }
      format.all  { render nothing: true, status: 404 }
    end
  end

  def server_error
    respond_to do |format|
      format.html { render template: 'errors/server_error', layout: 'layouts/shop', status: 500 }
      format.all  { render nothing: true, status: 500}
    end
  end

  private

  def load_menu
    @contemporary_products = ProductCode.contemporary_active
    @classic_products = ProductCode.classic_active

    @contemporary_designs = Design.contemporary.
      where("products_count > 0").includes(:products).
      where("products.accent_id IS NULL AND
        products.photo_file_name IS NOT NULL AND
        products.price IS NOT NULL")
    @classic_designs = Design.classic.
      where("products_count > 0").includes(:products).
      where("products.accent_id IS NULL AND
        products.photo_file_name IS NOT NULL AND
        products.price IS NOT NULL")

    @classic_designs << "accents"
    @contemporary_designs << "accents"

    @max_products_column_length =
      ([@contemporary_products.length, @classic_products.length].max/3.0).ceil
    @max_designs_column_length =
      ([@contemporary_designs.length, @classic_designs.length].max/3.0).ceil

    if pcont_menu_item =  MenuImage.products_contemporary.enabled.limited.first
      @products_contemporary_img = pcont_menu_item.photo.url(:menu)
    else
      @products_contemporary_img =  Design.contemporary.has_photo.offset(
        rand(Design.contemporary.has_photo.count)).first.photo.url(:preview)
    end
    if pclass_menu_item = MenuImage.products_classic.enabled.limited.first
      @products_classic_img = pclass_menu_item.photo.url(:menu)
    else
      @products_classic_img =  Design.classic.has_photo.offset(
        rand(Design.classic.has_photo.count)).first.photo.url(:preview)
    end
    if ccont_menu_item = MenuImage.collections_contemporary.enabled.limited.first
      @collection_contemporary_img = ccont_menu_item.photo.url(:menu)
    else
      @collection_contemporary_img = Design.contemporary.has_photo.offset(
        rand(Design.contemporary.has_photo.count)).first.photo.url(:preview)
    end
    if cclass_menu_item = MenuImage.collections_classic.enabled.limited.first
      @collection_classic_img = cclass_menu_item.photo.url(:menu)
    else
      @collection_classic_img = Design.classic.has_photo.offset(
        rand(Design.classic.has_photo.count)).first.photo.url(:preview)
    end
  end
end
