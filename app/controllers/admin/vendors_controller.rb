class Admin::VendorsController < Admin::BaseController
  respond_to :html, :json
  def index
    if params[:sort_key]
      @vendors = Vendor.order(params[:sort_key])
    else
      @vendors = Vendor.scoped
    end
    @vendors = @vendors.paginate :page => params[:page]
  end

  def create
    @vendor = Vendor.new(params[:vendor])
    if @vendor.save
      render :show
    else
      render :json => {:errors => @vendor.errors}, :status => :internal_server_error
    end
  end

  def show
    @vendor = Vendor.find(params[:id])
  end

  def update
    @vendor = Vendor.find(params[:id])
    if @vendor.update_attributes(params[:vendor])
      render :show
    else
      render :json => {:errors => @vendor.errors}, :status => :internal_server_error
    end
  end

end