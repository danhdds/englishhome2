class Admin::ProductsController < Admin::BaseController
  respond_to :html, :json
  def index
    @sort_direction = params[:sort_direction] || "asc"
    @sort_key= params[:sort_key] || "created_at"

    if params[:has_price]
      @products = Product.no_price.paginate(:page => params[:page])
      @conditions = []
      @facets = []
    elsif params[:no_short_code]
      @products = Product.no_short_code.paginate(:page => params[:page])
      @conditions = []
      @facets = []
    else
      @conditions = params.reject {|p,k| !Product::FACET_ATTRIBUTES.include?(p.to_sym)}

      if params[:has_photo].to_s == "true"
        @conditions.merge!({:photo_file_size => 1..9999999999})
      elsif params[:has_photo].to_s == "false"
        @conditions.merge!({:photo_file_size => 0})
      end

      @products = Product.search(params[:search], :page => params[:page], :with => @conditions, :order => "#{@sort_key} #{@sort_direction}")

      Rails.logger.info @conditions
      Rails.logger.info params[:search]


      @facets = Product.process_facets(Product.facets params[:search], :with => @conditions, :per_page => 200)

    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @products }
      format.js { render :json => {:success => true, :data => @products, :pages => @products.total_pages} }
    end
  end

  def show
    @product = Product.find(params[:id])
    respond_with @product
  end

  def upload_photo
    @product = Product.find(params[:id])
    @product.update_attributes(:photo => params["files"].first)

  end
end
