class Admin::UsersController < Admin::BaseController
  respond_to :html, :json
  def index
    @users = User.order("first_name ASC").paginate :page => params[:page]
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      render :json => @user.to_json
    else
      render :json => {:errors => @user.errors}, :status => :internal_server_error
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      render :json => @user.to_json
    else
      render :json => {:errors => @user.errors}, :status => :internal_server_error
    end
  end

end