class Admin::UserSessionsController < Admin::BaseController
  skip_before_filter :require_user
  layout 'login'
  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])

    if @user_session.save
      redirect_to admin_root_url
    else
      Rails.logger.info @user_session.errors.inspect
      render :action => 'new'
    end
  end

  def destroy
    @user_session =  UserSession.find
    @user_session.destroy
    redirect_to admin_root_url
  end
end