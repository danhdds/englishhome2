class Admin::GalleryImagesController < Admin::BaseController
  respond_to :html, :json

  def index
    @gallery_images = GalleryImage.all(:order => "position ASC")
  end

  def new
    @gallery_image = GalleryImage.new
  end

  def edit
    @gallery_image = GalleryImage.find(params[:id])
  end

  def create
    @gallery_image = GalleryImage.new(params[:gallery_image])
    if @gallery_image.save
      redirect_to admin_gallery_image_path(@gallery_image)
    else
      render :action => 'new'
    end
  end

  def show
    @gallery_image = GalleryImage.find(params[:id])
  end

  def update
    @gallery_image = GalleryImage.find(params[:id])
    @gallery_image.update_attributes(params[:gallery_image])
    render :action => :show
  end

  def upload
    @gallery_image = GalleryImage.find(params[:id])
    @gallery_image.update_attributes(:image => params[:files].first)
  end


  def destroy
    @gallery_image = GalleryImage.find(params[:id])
    @gallery_image.destroy
    redirect_to [:admin, :gallery_images]
  end

  def sort
    GalleryImage.all.each do |gallery_image|
      gallery_image.position = params['gallery_image'].index(gallery_image.id.to_s) + 1
      gallery_image.save
    end
     render :nothing => true
  end

end
