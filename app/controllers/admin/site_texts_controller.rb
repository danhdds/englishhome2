class Admin::SiteTextsController < Admin::BaseController
  respond_to :html, :json

  def index
    @site_texts = SiteText.paginate :page => params[:page]
  end

  def show
    @site_text = SiteText.find(params[:id])
  end

  def create
    @site_text = SiteText.new(params[:site_text])
    if @site_text.save
      render :show
    else
      render :json => {:errors => @site_text.errors}, :status => :internal_server_error
    end
  end

  def update
    @site_text = SiteText.find(params[:id])
    if @site_text.update_attributes(params[:site_text])
      render :show
    else
      render :json => {:errors => @site_text.errors}, :status => :internal_server_error
    end
  end

  def destroy
    @site_text = SiteText.find(params[:id])
    @site_text.destroy

    respond_to do |format|
      format.html { redirect_to site_texts_url }
      format.json { head :no_content }
    end
  end
end
