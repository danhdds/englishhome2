class Admin::BaseController < ApplicationController
  layout "admin"
  before_filter :capture_return_url, :require_user, :instantiate_controller_and_action_names

  def instantiate_controller_and_action_names
    @current_action = action_name
    @current_controller = controller_name
  end
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end



  def capture_return_url
    if params[:return_url]
      flash[:return_url]=params[:return_url]
      Rails.logger.info("Recording return url: #{params[:return_url]}")
    end
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def require_user
    unless current_user
      store_location
      flash[:notice] = "You must be logged in to access this page"
      redirect_to admin_login_path
      return false
    end
  end

  def require_admin_user
    unless current_user && current_user.admin?

      flash[:notice] = "You must be an administrator to access this page"
      redirect_to admin_login_path
      return false
    end
  end


  def store_location

    session[:return_to] = request.fullpath
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end
end