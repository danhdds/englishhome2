class Admin::AttributesController < Admin::BaseController
  respond_to :html, :json
  def index
    @attribute_types = ATTRIBUTE_TYPES
    @attributes = ATTRIBUTE_TYPES.inject({}) do |memo, attribute_type|
      memo[attribute_type] = attribute_type.constantize.all
      memo
    end
  end

  def show
    @attribute = Attribute.find(params[:id])
    respond_with @attribute
  end

end