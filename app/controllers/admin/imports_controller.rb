class Admin::ImportsController < Admin::BaseController
  respond_to :html, :json

  def index
    @imports = Import.paginate :page => params[:page], :order => "created_at DESC"
  end

  def import_image
    if params[:import]
      import_params = params[:import]
      @import = ImageImport.new(import_params.merge(:user => current_user))
      if !@import.save
        render :json => @import.errors, status: :unprocessable_entity
      end
    else
      errors = {:errors => "Couldn't upload image. Please try again."}
      render_errors errors, :internal_server_error
    end
  end

  def import_price_list
    begin
      import_params = params[:import]
      @import = PriceListImport.new(import_params.merge(:user => current_user))

      if !@import.save
        render_errors @import.errors, :unprocessable_entity
      end
    rescue TypeError => type_error
      errors = {:errors => type_error.message}.to_json
      render_errors errors, :internal_server_error
    rescue StandardError => error
      errors = {:errors => error.message}.to_json
      render_errors errors, :internal_server_error
    end
  end

  def render_errors(errors, status)
    render :json => errors, :status => status
  end
end