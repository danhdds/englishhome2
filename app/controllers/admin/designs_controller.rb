class Admin::DesignsController < Admin::BaseController
  respond_to :html, :json
  def index
    @sort_direction = params[:sort_direction] || "asc"
    @sort_key= params[:sort_key] || "name"

    @designs = Design.paginate(:page => params[:page]).reorder("#{@sort_key} #{@sort_direction}")
  end

  def moodshot
    @design = Design.find(params[:id])
    @design.update_attributes(:photo => params["files"].first)
  end

  def list_photo
    @design = Design.find(params[:id])
    @design.update_attributes(:list_photo => params["files"].first)
  end

  def show
  end

end
