class Admin::MenuImagesController < Admin::BaseController
  respond_to :html, :json

  def index
    @menu_images = MenuImage.paginate(:page => params[:page])
  end

  def upload_photo
    @menu_image = MenuImage.find(params[:id])
    @menu_image.update_attributes(:photo => params["files"].first)
  end

  def show
    @menu_image = MenuImage.find(params[:id])
  end


  def create
    @menu_image = MenuImage.new(params[:menu_image])
    if @menu_image.save
      render :show
    else
      render :json => {:errors => @menu_image.errors}, :status => :internal_server_error
    end
  end

  def update
    @menu_image = MenuImage.find(params[:id])
    if @menu_image.update_attributes(params[:menu_image])
      render :show
    else
      render :json => {:errors => @menu_image.errors}, :status => :internal_server_error
    end
  end
end
