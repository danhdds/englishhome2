class CollectionsController < ShopController

  def index
    @images = GalleryImage.to_be_shown.all(:order => "position ASC")
  end

  def show
    if params[:id] == "accents"
      case params[:style]
      when 'contemporary'
        style_conditions = " AND products.collection_type = 0"
        @title = "Contemporary Accent cushion covers"
      when 'classic'
        style_conditions = " AND products.collection_type = 1"
        @title = "Classic Accent cushion covers"
      else
        @title = "Accent cushion covers"
      end

      joins_condition = "INNER JOIN products ON
        products.product_code_id = attributes.id AND
        products.accent_id IS NOT NULL AND
        products.photo_file_name IS NOT NULL AND
        products.price IS NOT NULL"
      joins_condition += style_conditions if style_conditions


      @product_types = ProductCode.all(
        :select => "attributes.*, count(products.id) as product_count",
        :joins => joins_condition,
        :group => "attributes.id",
        :order => "product_code_position ASC")
    else
      @design = Design.find(params[:id])
      @title = "#{@design.name} collection"
      @product_types = ProductCode.all(
        :select => "attributes.*, count(products.id) as product_count",
        :joins => "INNER JOIN products ON
          products.product_code_id = attributes.id AND
          products.design_id = #{@design.id} AND
          products.photo_file_name IS NOT NULL AND
          products.price IS NOT NULL",
        :group => "attributes.id",
        :order => "product_code_position ASC")
    end




    @products = []
    @product_types.each do |product_type|
      if @design
        conditions = ["design_id = ? AND
                      photo_file_name IS NOT NULL AND
                      products.price IS NOT NULL", @design.to_param]
      else
        conditions = ["accent_id IN (?) AND
                      photo_file_name IS NOT NULL AND
                      products.price IS NOT NULL",
                      Product.unscoped.pluck(:accent_id).uniq.compact]
        conditions[0] += style_conditions if style_conditions
      end
      @products.concat product_type.products.find(:all, :conditions => conditions)
    end

    @product_rows = [[]]
    @products.compact.uniq.each do |product|
      if @product_rows.last.size==3
        @product_rows << []
      end
      @product_rows.last << product
    end
    if @product_rows.count == 1 && @product_rows.first.count != 3
      @product_rows.first << "empty" if @product_rows.first.count == 1
      @product_rows.first << "empty" if @product_rows.first.count == 2
    end
  end

  def contemporary
    @collections = Design.contemporary.
      where("products_count > 0").includes(:products).
      where("products.accent_id IS NULL AND
        products.photo_file_name IS NOT NULL AND
        products.price IS NOT NULL")
    @collection_rows = [[]]
    @title = "Collections"
    for collection in @collections
      if collection.products.count >0
        if @collection_rows.last.size==3
          @collection_rows << []
        end

        @collection_rows.last << collection
      end
    end
  end

  def classic
    @collections = Design.classic.
      where("products_count > 0").includes(:products).
      where("products.accent_id IS NULL AND
      products.photo_file_name IS NOT NULL AND
      products.price IS NOT NULL")
    @collection_rows = [[]]
    @title = "Collections"
    for collection in @collections
      if collection.products.count >0
        if @collection_rows.last.size==3
          @collection_rows << []
        end

        @collection_rows.last << collection
      end
    end
  end
end
