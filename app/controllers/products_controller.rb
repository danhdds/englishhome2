class ProductsController < ShopController
  respond_to :html

  def show
    @product = Product.find(params[:id])
    if @product.present? && @product.product_code.present?
      @title = @product.name
      respond_with @product
    else
      redirect_to shop_path
    end
  end

  def search
    @data = Product.process_facets(Product.facets(params[:search], :with => {
      :photo_file_size => 1..1000000000,
      :price => 1.0..9999999999999.9})
      ).select {|facet, options| facet == :product_code_id}

    @products = []
    @data.each do |option_name, options|
      options.each do |option, count|
        products = option.products.search(params[:search],
          :with => {:photo_file_size => 1..1000000000,
                    :price => 1.0..9999999999999.9}, :per_page => 200)
        @products.concat products.to_a
      end
    end

    @product_rows = [[]]

    for product in @products
      if @product_rows.last.size == 3
        @product_rows << []
      end

      @product_rows.last << product
    end

    if @product_rows.count == 1 && @product_rows.first.count != 3
      @product_rows.first << "empty" if @product_rows.first.count == 1
      @product_rows.first << "empty" if @product_rows.first.count == 2
    end

  end
end
