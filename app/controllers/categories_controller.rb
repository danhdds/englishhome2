class CategoriesController < ShopController

  def index
    @title = "Categories"
    @categories = ProductCode.active
    @category_rows = [[]]

    for category in @categories

        if @category_rows.last.size==3
          @category_rows << []
        end

        @category_rows.last << category

    end
  end

  def show

    @category = ProductCode.find(params[:id])
    @title = @category.name.pluralize

    params.reject! {|p,k| k.blank?}

    @conditions = {:product_code_id => @category.id}.merge(params.reject {|p,k| !Product::FACET_ATTRIBUTES.include?(p.to_sym)})
    @conditions.merge!(:collection_type => (params['collection'].titlecase == "Classic")) if params['collection']
    @conditions.merge!(:photo_file_size => 1..1000000000) # Max Filesize?
    @conditions.merge!(:price => 1.0..999999999999.9) # Max Price?
    Rails.logger.info("\n\n----> Conditions: #{@conditions}")

    if params[:view_all]
      @products = Product.search :with => @conditions, :page => params[:page], :per_page => 500, :order => "name ASC"
    else
      @products = Product.search :with => @conditions, :page => params[:page], :order => "name ASC", :per_page => 12
    end
    @facets = Product.process_facets(Product.facets :with => @conditions, :per_page => 200)

    @product_rows = [[]]

    for product in @products
      if @product_rows.last.size == 3
        @product_rows << []
      end

      @product_rows.last << product
    end

    if @product_rows.count == 1 && @product_rows.first.count != 3
      @product_rows.first << "empty" if @product_rows.first.count == 1
      @product_rows.first << "empty" if @product_rows.first.count == 2
    end
  end
end
