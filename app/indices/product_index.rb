ThinkingSphinx::Index.define :product, :with => :active_record, :delta => ThinkingSphinx::Deltas::DelayedDelta do
      # fields
    indexes name, :sortable => true
    indexes code, :sortable => true
    indexes enabled, :sortable => true
    indexes deleted_at, :sortable => true

    indexes short_code, :sortable => true
    set_property :delta => true
    has position, :sortable => true
    has created_at, :sortable => true
    has price, :sortable => true
    has collection_type, :sortable => true
    has photo_file_size, :sortable => true

    #facets
    Product::FACET_ATTRIBUTES.each do |attribute|
      has attribute, :facet => true
    end


end