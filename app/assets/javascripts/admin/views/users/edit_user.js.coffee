class Admin.Views.EditUserView extends Admin.Views.PropertiesView
  template: JST['admin/templates/edit_user']

  events:
    "click .close": "close"
    "click .btn-submit" : "saveUser"

  className: 'popover right user'

  renderDetails: () =>
    super
    @$(".first_name").val(@model.get('first_name')) if @model.get('first_name')?
    @$(".last_name").val(@model.get('last_name')) if @model.get('last_name')?
    @$(".email").val(@model.get("email")) if @model.get("email")?
    @$(".bio").text(@model.get("bio")) if @model.get("bio")?
    super

  getRow: () =>
    $("#clickable_for_user_#{@model.id}")

  title: () =>
    "Edit User"

  saveUser: () =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    @model.save {
      first_name: @$(".first_name").val()
      last_name: @$(".last_name").val()
      email: @$(".email").val()
      password: @$(".password").val()
      password_confirmation: @$(".pass-conf").val()
      bio: @$(".bio").val()
      },
      success: () =>
        window.location.hash = "#/users/#{@model.id}"
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")
