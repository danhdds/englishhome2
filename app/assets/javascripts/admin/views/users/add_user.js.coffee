class Admin.Views.AddUserView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_user']

  events:
    "click .close": "close"
    "click .btn-submit" : "createUser"

  className: 'popover bottom add-user'

  render: () =>
    @$el.html @template()
    @$(".title").text(@title())

  highlight: () =>
    #no-op
  getRow: () =>
    #no-op
  getModel: () =>
    #no-op
  scroll: () =>

  title: () =>
    "Create User"

  createUser: () =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    user = new Admin.Models.User
      first_name: @$(".first_name").val()
      last_name: @$(".last_name").val()
      email: @$(".email").val()
      password: @$(".password").val()
      password_confirmation: @$(".pass-conf").val()
      bio: @$(".bio").val()
    user.save {},
      success: () =>
        window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")
