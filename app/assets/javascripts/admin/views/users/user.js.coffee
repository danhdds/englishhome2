#= require '../properties'
class Admin.Views.UserView extends Admin.Views.PropertiesView
  template: JST['admin/templates/user']

  events:
    "click .close": "close"

  className: 'popover right user'

  renderDetails: () =>
    super
    @$(".name").text(@model.wholeName())
    @$(".email").text(@model.get("email"))
    @$(".created_at").text(@model.createdAt())
    @$(".last-login-in").text(@model.lastLoginAt())
    @$(".bio").text(@model.get("bio"))

    if @model.get('mugshot_img_url')
      @$("#mugshot-upload img").attr
        src: @model.get('mugshot_img_url')
    else
      @$("#mugshot-upload img").attr
        src: "/assets/logo.png"

    super

  render: () =>
    @$el.html @template(model: @model)

    @$('#mugshot-upload').fileupload
        dropZone: @$('#mugshot-upload')
        dataType: 'json'
        url: "/admin/user/#{@model.id}/mugshot.json",
        done: (e, data) =>
          console.log("Done")
          @getModel()
          # $.each data.result.files, (index, importImageJSON) =>
            # @images.add(new Admin.Models.Import(importImageJSON, {parse: true}))

  getRow: () =>
    $("#clickable_for_user_#{@model.id}")

  title: () =>
    "User Details"