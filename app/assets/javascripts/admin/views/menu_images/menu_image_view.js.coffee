#= require './../properties'
class Admin.Views.MenuImageView extends Admin.Views.PropertiesView
  template: JST['admin/templates/menu_image']

  events:
    "click .close"                 : "close"
    "change input[name='location']"  : "setLocation"
    "change .enabled"              : "toggleEnabled"

  className: 'popover right gallery-image'

  renderDetails: =>
    super
    @$('.enabled input').prop("checked", true) if @model.get("enabled")
    @$('.p-classic input').prop("checked", true) if @model.
      get('products_classic')
    @$('.p-contemporary input').prop("checked", true) if @model.
      get('products_contemporary')
    @$('.c-classic input').prop("checked", true) if @model.
      get('collections_classic')
    @$('.c-contemporary input').prop("checked", true) if @model.
      get('collections_contemporary')

    if @model.get('preview_img_url')
      @$("#moodshot-upload img").attr
        src: @model.get('preview_img_url')
    else
      @$("#moodshot-upload img").attr
        src: "/assets/logo.png"

    super

  toggleEnabled: =>
    @model.set "enabled", !@model.get("enabled")
    @save()

  setLocation: (evt) =>
    value = @$(evt.target).val()
    switch value
      when 'products_classic'
        @model.set 'products_classic', true
        @model.set 'products_contemporary', false
        @model.set 'collections_classic', false
        @model.set 'collections_contemporary', false
      when 'products_contemporary'
        @model.set 'products_classic', false
        @model.set 'products_contemporary', true
        @model.set 'collections_classic', false
        @model.set 'collections_contemporary', false
      when 'collections_classic'
        @model.set 'products_classic', false
        @model.set 'products_contemporary', false
        @model.set 'collections_classic', true
        @model.set 'collections_contemporary', false
      when 'collections_contemporary'
        @model.set 'products_classic', false
        @model.set 'products_contemporary', false
        @model.set 'collections_classic', false
        @model.set 'collections_contemporary', true
    @save()

  save: =>
    @model.save null,
      patch: true

  render: =>
    @$el.html @template()

    @$('#moodshot-upload').fileupload
      dropZone: @$('#moodshot-upload')
      dataType: 'json'
      url: "/admin/menu_image/#{@model.id}/upload_photo.json",
      done: (e, data) =>
        console.log("Done")
        @getModel()

  getRow: =>
    $("#clickable_for_menu_image_#{@model.id}")

  title: ->
    "Menu Image"
