class Admin.Views.AddMenuImageView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_menu_image']

  events:
    "click .close"                 : "close"
    "change input[name='location']"  : "setLocation"
    "change .enabled"              : "toggleEnabled"

  className: 'popover bottom add-gallery-image'

  initialize: =>
    @model = new Admin.Models.MenuImage()

  highlight: ->
    #no-op
  getRow: ->
    #no-op
  getModel: ->
    #no-op
  scroll: ->

  title: ->
    "Add Menu Image"

  createMenuImage: =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    @model.save {},
      success: ->
        window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")

  attributeName: (attributeName) =>
    attribute = @model.get(attributeName)
    if attribute?
      "#{attribute.name}"

  toggleEnabled: =>
    @model.set "enabled", !@model.get("enabled")

  setLocation: (evt) =>
    value = @$(evt.target).val()
    switch value
      when 'products_classic'
        @model.set 'products_classic', true
        @model.set 'products_contemporary', false
        @model.set 'collections_classic', false
        @model.set 'collections_contemporary', false
      when 'products_contemporary'
        @model.set 'products_classic', false
        @model.set 'products_contemporary', true
        @model.set 'collections_classic', false
        @model.set 'collections_contemporary', false
      when 'collections_classic'
        @model.set 'products_classic', false
        @model.set 'products_contemporary', false
        @model.set 'collections_classic', true
        @model.set 'collections_contemporary', false
      when 'collections_contemporary'
        @model.set 'products_classic', false
        @model.set 'products_contemporary', false
        @model.set 'collections_classic', false
        @model.set 'collections_contemporary', true


  render: =>
    @$el.html @template()
    @$(".title").text(@title())

    @$('#moodshot-upload').fileupload
      dropZone: @$('#moodshot-upload')
      add: (e, data) =>
        console.log data
        if data.files?[0]
          reader = new FileReader()

          reader.onload = (e) =>
            @$("#moodshot-upload img").attr "src", e.target.result

          reader.readAsDataURL data.files[0]
          data.context = @$(".btn-submit").removeClass('hidden').click( =>
            @model.save({},
              success: =>
                data.url = "/admin/menu_images/#{@model.id}/upload_photo.json"
                data.submit()
            )
          )
      dataType: 'json'
      done: (e, data) ->
        console.log("Done")
        window.location.reload()
