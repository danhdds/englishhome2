#= require './properties'
class Admin.Views.DesignView extends Admin.Views.PropertiesView
  template: JST['admin/templates/design']

  events:
    "click .close": "close"

  className: 'popover right design'

  renderDetails: =>
    super
    @$(".name").text(@model.get('name'))
    @$(".code").text(@model.get('code'))
    @$(".products").text(@model.get('products'))

    @$(".created_at").text(@model.createdAt())

    @renderField "code", @model.get("code")
    @renderField "detail", @model.get("detail")
    @renderField "metric_description", @model.get("metric_description")
    @renderField "imperial_description", @model.get("imperial_description")
    @renderField "description", @model.get("description")
    @renderField "dimension", @model.get("dimension")
    @renderField "plural", @model.get("plural")
    @renderField "present_in_last_import", @model.get("present_in_last_import")
    @renderField "collection-type", @model.get("collection_type")
    console.log @model
    if @model.get('photo_img_url')
      @$("#moodshot-upload img").attr
        src: @model.get('photo_img_url')
    else
      @$("#moodshot-upload img").attr
        src: "/assets/logo.png"

    if @model.get("list_photo_img_url")
      @$("#list-photo-upload img").attr
        src: @model.get("list_photo_img_url")
    else
      @$("#list-photo-upload img").attr
        src: "/assets/logo.png"

    super

  render: =>
    @$el.html @template()
    console.log "/admin/designs/#{@model.id}/moodshot.json"
    @$('#moodshot-upload').fileupload
      dropZone: @$('#moodshot-upload')
      dataType: 'json'
      url: "/admin/designs/#{@model.id}/moodshot.json"
      done: (e, data) =>
        console.log("Done")
        @getModel()
    @$('#list-photo-upload').fileupload
      dropZone: @$('#list-photo-upload')
      dataType: 'json'
      url: "/admin/designs/#{@model.id}/list_photo.json"
      done: (e, data) =>
        console.log("Done")
        @getModel()


  getRow: =>
    $("#clickable_for_attribute_#{@model.id}")

  title: ->
    "Design Details"
