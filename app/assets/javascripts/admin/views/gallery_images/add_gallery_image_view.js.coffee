class Admin.Views.AddGalleryImageView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_gallery_image']

  events:
    "click .close": "close"
    "change .collection-select": "updateCollection"
    "change .show-gallery": "toggleGallery"

  className: 'popover bottom add-gallery-image'

  initialize: () =>
    @model = new Admin.Models.GalleryImage()

  highlight: () =>
    #no-op
  getRow: () =>
    #no-op
  getModel: () =>
    #no-op
  scroll: () =>

  title: () =>
    "Add New Gallery Image"

  createGalleryImage: () =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    @model.save {},
      success: () =>
        window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")

  attributeName: (attributeName) =>
    attribute = @model.get(attributeName)
    if attribute?
      "#{attribute.name}"

  toggleGallery: () =>
    @model.set "home_page", !@model.get("home_page")

  updateCollection: (e) =>
    collection = @$(e.currentTarget).select2("data")
    if collection?
      console.log collection.name
      console.log collection.id
      @model.set e.currentTarget.name, collection.id
    else
      @model.set e.currentTarget.name, null

  render: () =>
    @$el.html @template()
    @$(".title").text(@title())

    Admin.Data.designs.each (design) =>
      @$(".collection-select").append $("<option>").html(design.name).attr
        "value": design.id

    @$(".collection-select").select2
      placeholder: "Select a collection"
      allowClear: true

    @$('#moodshot-upload').fileupload
        dropZone: @$('#moodshot-upload')
        add: (e, data) =>
          console.log data
          if data.files?[0]
            reader = new FileReader()

            reader.onload = (e) =>
              @$("#moodshot-upload img").attr "src", e.target.result

            reader.readAsDataURL data.files[0]
            data.context = @$(".btn-submit").removeClass('hidden').click(() =>
              @model.save({},
                success: () =>
                  data.url = "/admin/gallery_images/#{@model.id}/upload.json"
                  data.submit()
              )
            )
        dataType: 'json'
        done: (e, data) =>
          console.log("Done")
          window.location.reload()
        #   @getModel()
          # $.each data.result.files, (index, importImageJSON) =>
            # @images.add(new Admin.Models.Import(importImageJSON, {parse: true}))

