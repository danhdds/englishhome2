class ClickableRow extends Backgrid.Row
  events:
    "click": "onClick"

  onClick: () =>
    Backbone.trigger("rowclicked", @model)

  initialize: (options) =>
    super options
    @$el.attr
      id: "clickable_for_gallery_image_#{@model.id}"


class Admin.Views.GalleryImagesView extends  Support.CompositeView
  template: JST['admin/templates/gallery_images']


  gridColumns: =>
    [

      name: "id"
      label: ""
      editable: false
      cell: Backgrid.StringCell.extend
        render: () ->
          img_attrs = { src: @model.get("admin_img_url"), width: 24, height: 24 }

          @$el.empty()
          @$el.addClass("image")
          @$el.append $("<img>", img_attrs)
          @delegateEvents()
          @


    ,
      name: "image_file_name"
      label: "Filename"
      editable: false
      cell: 'string'
      sortable: true
    ,
      name: "collection1_id"
      label: "Collections"
      editable: false
      cell: Backgrid.StringCell.extend
        render: () ->
          @$el.empty()
          collections = _.compact [@model.get("collection1"), @model.get("collection2"), @model.get("collection3")]
          c = _.map collections, (c) =>
            c.name.trim()
          @$el.text(c.join(", "))
          @delegateEvents()
          @
    ]




  initialize: =>
    Admin.Data.gallery_images = new Admin.Collections.GalleryImages()
    Admin.Data.gallery_images.fetch()
    console.log @gridColumns()
    @grid = new Backgrid.Grid
      className: "backgrid table-list"
      row: ClickableRow
      columns: @gridColumns(),
      collection: Admin.Data.gallery_images

    Backbone.on "rowclicked", (model) =>
      Admin.app.navigate "gallery_images/#{model.id}",
        trigger: true


  render: () =>
    @$el.html @template()
    @$(".list").append(@grid.render().$el)
    @$(".backgrid tbody" ).sortable
      axis: "y"
      helper: (e, ui) =>
        originals = ui.children()

        helper = ui.clone()
        children = helper.children()
        children.each (index) ->

          console.log originals.eq(index).width()
          $(@).width(originals.eq(index).width())
          console.log index, @
        console.log helper
        return helper
      cursor: 'pointer'
      update: () =>
        $.ajax
          type: 'post'
          data: @$(this).sortable('serialize')
          dataType: 'script'

          url: "TEST"
    @$(".backgrid tbody").disableSelection()


