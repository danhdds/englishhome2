#= require './../properties'
class Admin.Views.GalleryImageView extends Admin.Views.PropertiesView
  template: JST['admin/templates/gallery_image']

  events:
    "click .close": "close"
    "change .collection-select": "updateCollection"
    "change .show-gallery": "toggleGallery"

  className: 'popover right gallery-image'

  renderDetails: () =>
    super
    # @$(".name").text(@model.get('name'))
    @$(".code").text(@model.get('code'))
    @$(".products").text(@model.get('products'))

    @$(".created_at").text(@model.createdAt())

    @renderField "filename", @model.get("image_file_name")

    @$("select.collection-1").select2("val", @model.get("collection1_id"))
    @$("select.collection-2").select2("val", @model.get("collection2_id"))
    @$("select.collection-3").select2("val", @model.get("collection3_id"))

    @$('.show-gallery input').prop("checked", true) if @model.get("home_page")

    if @model.get('preview_img_url')
      @$("#moodshot-upload img").attr
        src: @model.get('preview_img_url')
    else
      @$("#moodshot-upload img").attr
        src: "/assets/logo.png"

    super

  attributeName: (attributeName) =>
    attribute = @model.get(attributeName)
    if attribute?
      "#{attribute.name}"

  updateCollection: (e) =>
    collection = @$(e.currentTarget).select2("data")
    if collection?
      console.log collection.name
      console.log collection.id
      @model.set e.currentTarget.name, collection.id
    else
      @model.set e.currentTarget.name, null
    @save()

  toggleGallery: () =>
    @model.set "home_page", !@model.get("home_page")
    @model.save()

  save: () =>

    console.log @model.changedAttributes()
    console.log @model.isNew()
    @model.save null,
      patch: true

  render: () =>
    @$el.html @template()

    Admin.Data.designs.each (design) =>
      @$(".collection-select").append $("<option>").html(design.name).attr
        "value": design.id

    @$(".collection-select").select2
      placeholder: "Select a collection"
      allowClear: true

    @$('#moodshot-upload').fileupload
        dropZone: @$('#moodshot-upload')
        dataType: 'json'
        url: "/admin/gallery_images/#{@model.id}/upload.json",
        done: (e, data) =>
          console.log("Done")
          @getModel()
          # $.each data.result.files, (index, importImageJSON) =>
            # @images.add(new Admin.Models.Import(importImageJSON, {parse: true}))

  getRow: () =>
    $("#clickable_for_gallery_image_#{@model.id}")

  title: () =>
    "Design Details"
