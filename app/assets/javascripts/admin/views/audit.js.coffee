#= require './properties'
class Admin.Views.AuditView extends Support.CompositeView
  template: JST['admin/templates/audit']

  events:
    "click .close": "close"

  className: 'history_item'
  tagName: 'tr'

  render: () =>
    console.log @
    @$el.html @template(model: @model)
