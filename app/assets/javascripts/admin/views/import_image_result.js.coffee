class Admin.Views.ImportImageResultView extends Support.CompositeView
  template: JST['admin/templates/import_image_result']

  tagName: 'div'

  initialize: () =>
    @product = @model.product

  render: () =>
    @$el.html @template(model: @model)
    @renderDetails()

  renderDetails: () =>
    @$(".name").text(@product.get('name'))
    @$(".code").text(@product.get('code')  || "Long code not set")
    @$(".short-code").text(@product.get('short_code')  || "Short code not set")
    @$(".price").text(@product.get('formatted_price'))
    if @product.get('preview_img_url')
      @$(".image-holder").show()
      @$("img").attr
        src: @product.get('preview_img_url')
      console.log  @product.get('dominant_colour')
      @$("img").css
        "border-color": @product.get('dominant_colour')

    else
      @$(".image-holder").hide()

