class Admin.Views.PropertiesView extends Support.CompositeView
  template: JST['admin/templates/properties']

  events:
    "click .close": "close"

  className: 'popover right product'

  initialize: (attributes, options) =>
    @getModel()

  getModel: () =>
    @model.fetch
      success: () =>
        @renderDetails()

  getRow: () =>
    $("#clickable_for_product_#{@model.id}")

  highlight: () =>
    if @isShown
      @row = @getRow()
      @offset = @row.offset().top + @row.height() / 2
      console.log @row, @offset
      @row.addClass("highlighted")
    else
      @row?.removeClass("highlighted")

  render: () =>
    @$el.html @template(model: @model)

  renderDetails: () =>
    @$(".title").text(@title())
    @$(".details").removeClass("hidden")

  title: () =>
    "Unnamed Panel"

  renderField: (fieldName, value) =>
    if value? and value != ''
      @$(".details .#{fieldName}").parents(".hidden").removeClass("hidden")
      @$(".details .#{fieldName}").text(value)

  show: () =>
    @render()
    if !@$el.is(":visible")
      @$el.addClass("shown")
    $('.sidebar').append(@el)
    @isShown = true
    @escape()
    @highlight()
    @scroll()


  hide: () =>
    @isShown = false
    @leave()
    @escape()
    @scroll()
    @highlight()


  close: () =>
    Admin.app.navigate "#",
      trigger: false
      replace: true
    @hide()

  escape:  () =>
    if @isShown
      $("body").on 'keyup.dismiss.modal', (e) =>
        e.which is 27 and @close()
    else
      $("body").off 'keyup.dismiss.modal'

  scroll: () =>
    if @isShown
      lazyrepositionArrow = _.debounce(@_repositionArrow, 100)
      $(window).on 'scroll.modal resize.modal', (e) =>
        @$(".arrow").addClass("transition")
        lazyrepositionArrow()
      @_repositionArrow()
    else
      $(window).off 'scroll.modal resize.modal'


  _repositionArrow: () =>
    @offset = @row.offset().top + @row.height() / 2
    @arrowOffset = @offset - @$el.offset().top
    if @arrowOffset > 40 and @arrowOffset < @$el.height()
      @$(".arrow").css
        "top": @arrowOffset
        "opacity": 1
    else
      @$(".arrow").css
        "opacity": 0

