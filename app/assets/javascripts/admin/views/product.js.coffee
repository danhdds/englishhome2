#= require './properties'
class Admin.Views.ProductView extends Admin.Views.PropertiesView
  template: JST['admin/templates/product']

  events:
    "click .close": "close"
    "click #properties-tab a": "showProperties"
    "click #history-tab a": "showHistory"

  className: 'popover right product'

  initialize: () =>
    super
    @auditViews = _([])

  renderDetails: () =>
    @renderAudits()
    @$(".name").text(@model.get('name'))
    @$(".code").text(@model.get('code')  || "Long code not set")
    @$(".short-code").text(@model.get('short_code')  || "Short code not set")
    @$(".price").text(@model.get('formatted_price'))

    @$(".created_at").text(@model.createdAt())

    @$(".type").text              @attributeName("product_code")
    @$(".enabled").text           @model.get("enabled")
    @$(".base_fabric").text       @attributeName("base_fabric")
    @$(".design").text            @attributeName("design")
    @$(".shape").text             @attributeName("shape")
    @$(".base_colour").text       @attributeName("base_colour")
    @$(".applique1").text         @attributeName("applique1")
    @$(".applique2").text         @attributeName("applique2")
    @$(".applique3").text         @attributeName("applique3")
    @$(".with").text              @attributeName("with_option")
    @$(".and").text               @attributeName("and_option")

    @$(".other1").text            @attributeName("other1")
    @$(".other2").text            @attributeName("other2")
    @$(".dimension").text         @attributeName("dimension")

    console.log @model

    if @model.get('preview_img_url')
      @$("#photo-upload img").attr
        src: @model.get('preview_img_url')
    else
      @$("#photo-upload img").attr
        src: "/assets/logo.png"

    super

  render: () =>
    @$el.html @template()
    console.log "/admin/products/#{@model.id}/upload_photo.json"
    @$('#photo-upload').fileupload
        dropZone: @$('#photo-upload')
        dataType: 'json'
        url: "/admin/products/#{@model.id}/upload_photo.json",
        done: (e, data) =>
          console.log("Done")
          @getModel()

  renderAudits: () =>
    @clearAudits()

    @model.audits.each (audit) =>
      auditView = new Admin.Views.AuditView(model: audit)
      @auditViews.push auditView
      @appendChildTo auditView, ".audits"

  clearAudits: () =>
    @auditViews.each (auditView) =>
      auditView.leave()

  attributeName: (attributeName) =>
    attribute = @model.get(attributeName)
    if attribute?
      "#{attribute.name}, #{attribute.code}"

  getRow: () =>
    $("#clickable_for_product_#{@model.id}")

  title: () =>
    @model.get('name')

  showHistory: (e) =>
    e?.preventDefault()
    @$("#history-tab a").tab('show')

  showProperties: (e) =>
    e?.preventDefault()
    @$("#properties-tab a").tab('show')
