#= require './properties'
class Admin.Views.AttributeView extends Admin.Views.PropertiesView
  template: JST['admin/templates/attribute']

  events:
    "click .close": "close"

  className: 'popover right product'

  renderDetails: () =>
    super
    @$(".name").text(@model.get('name'))
    @$(".code").text(@model.get('code'))
    @$(".products").text(@model.get('products'))

    @$(".created_at").text(@model.createdAt())

    @renderField "colour", @model.get("colour")
    @renderField "detail", @model.get("detail")
    @renderField "metric_description", @model.get("metric_description")
    @renderField "imperial_description", @model.get("imperial_description")
    @renderField "description", @model.get("description")
    @renderField "dimension", @model.get("dimension")
    @renderField "plural", @model.get("plural")
    @renderField "present_in_last_import", @model.get("present_in_last_import")
    @renderField "collection-type", @model.get("collection_type")



  getRow: () =>
    $("#clickable_for_attribute_#{@model.id}")

  title: () =>
    "Attribute Details"