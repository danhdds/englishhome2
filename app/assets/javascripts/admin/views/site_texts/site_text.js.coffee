#= require '../properties'
class Admin.Views.SiteTextView extends Admin.Views.PropertiesView
  template: JST['admin/templates/site_text']

  events:
    "click .close": "close"

  className: 'popover right user'

  renderDetails: =>
    super
    @$(".text").html(@model.get("text").replace(/\r\n|\r|\n/g,"<br />"))
    @$('.enabled input').prop('checked', true) if @model.get("enabled")
    @$('.trade input').prop('checked', true) if @model.get('trade')
    @$('.about_us input').prop('checked', true) if @model.get('about_us')
    @$('.shipping input').prop('checked', true) if @model.get('shipping')
    super

  getRow: =>
    $("#clickable_for_site_text_#{@model.id}")

  title: ->
    "Site Text"
