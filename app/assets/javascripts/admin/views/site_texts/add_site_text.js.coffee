class Admin.Views.AddSiteTextView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_site_text']

  events:
    "click .close": "close"
    "click .btn-submit" : "createSiteText"

  className: 'popover bottom add-user'

  render: =>
    @$el.html @template()
    @$(".title").text(@title())

  highlight: ->
    #no-op
  getRow: ->
    #no-op
  getModel: ->
    #no-op
  scroll: ->

  title: ->
    "Create Site Text"

  createSiteText: =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    unless @$('input[name="location"]:checked').val()
      @$(".errors").removeClass("hidden")
      @$(".errors").html("Either About us or Trade needs to be selected")
      return

    radio_value = @$('input[name="location"]:checked').val()
    switch radio_value
      when "about_us"
        about_us = true
        trade = false
      when "trade"
        about_us = false
        trade = true
      when "shipping"
        about_us = false
        trade = false
        shipping = true

    site_text = new Admin.Models.SiteText(
      text: @$(".text").val()
      enabled: @$('.enabled input').prop('checked')
      trade: trade
      about_us: about_us
      shipping: shipping)
    site_text.save {},
      success: ->
        window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")
