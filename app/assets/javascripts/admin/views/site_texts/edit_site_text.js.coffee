class Admin.Views.EditSiteTextView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_site_text']

  events:
    "click .close": "close"
    "click .btn-submit" : "saveSiteText"

  className: 'popover right user'

  render: =>
    @$el.html @template()
    @$(".title").text(@title())

  renderDetails: =>
    super
    @$(".text").text(@model.get("text"))
    @$('.enabled input').prop('checked', true) if @model.get("enabled")
    @$('.trade input').prop('checked', true) if @model.get('trade')
    @$('.about_us input').prop('checked', true) if @model.get('about_us')
    @$('.shipping input').prop('checked', true) if @model.get('shipping')
    super

  getRow: =>
    $("#clickable_for_site_text_#{@model.id}")

  title: ->
    "Edit Text"

  saveSiteText: =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    unless @$('input[name="location"]:checked').val()
      @$(".errors").removeClass("hidden")
      @$(".errors").html("Either About us or Trade needs to be selected")
      return

    radio_value = @$('input[name="location"]:checked').val()
    switch radio_value
      when "about_us"
        about_us = true
        trade = false
        shipping = false
      when "trade"
        about_us = false
        trade = true
        shipping = false
      when "shipping"
        about_us = false
        trade = false
        shipping = true

    @model.save
      text: @$(".text").val()
      enabled: @$('.enabled input').prop('checked')
      trade: trade
      about_us: about_us
      shipping: shipping
    , success: ->
      window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")
