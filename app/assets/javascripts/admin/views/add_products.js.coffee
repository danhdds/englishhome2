#= require './properties'
class Admin.Views.AddProductsView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_products']

  events:
    "click .close": "close"

  className: 'popover bottom add-products'


  initialize: () =>
    @images = new Admin.Collections.Imports()
    @images.on("add", @imageUploaded)
    @importResultViews = _([])

  render: () =>
    @$el.html @template()
    @uploadCount = 0
    @finishedCount = 0

    @$('#images-upload').fileupload
        dataType: 'json'
        url: '/admin/imports/import_image.json',
        sequentialUploads: true
        add: (e, data) =>
          data.submit()
          @uploadCount++

          @$(".product-summary.error").addClass("hidden")
          @addSpinner @$("#images-upload").parents(".btn") unless @$('.spin-wrapper').length >= 1

        done: (e, data) =>
          $.each data.result.files, (index, importImageJSON) =>
            @images.add(new Admin.Models.Import(importImageJSON, {parse: true}))

          @finishedCount++

          if @finishedCount == @uploadCount
            @$(".spin-wrapper").remove()
            @$("#images-upload").parents(".btn").removeClass("disabled")

        error: (jqXHR, textStatus, errorThrown) =>
          @$(".error-area").html(JSON.parse(jqXHR.responseText).errors)
          @$(".product-summary.error").removeClass("hidden")

          @finishedCount++

          if @finishedCount == @uploadCount
            @$(".spin-wrapper").remove()
            @$("#images-upload").parents(".btn").removeClass("disabled")

    @$('#pricelist-upload').fileupload
        dataType: 'json'
        url: '/admin/imports/import_price_list.json',
        sequentialUploads: true
        add: (e, data) =>
          data.submit()

          @$(".product-summary.error").addClass("hidden")
          @addSpinner @$("#pricelist-upload").parents(".btn")
          @$("#pricelist-upload").parents(".btn").addClass("disabled")

        done: (e, data) =>
          @$("#pricelist-upload").parents(".btn").removeClass("disabled")
          $.each data.result.files, (index, file) =>
            $('<p />').text(file.name).appendTo(@$(".results"))
          @$(".spin-wrapper").remove()
          alert("Finished Upload! Please refresh.")

        error: (jqXHR, textStatus, errorThrown) =>
          @$("#pricelist-upload").parents(".btn").removeClass("disabled")
          @$(".error-area").html(JSON.parse(jqXHR.responseText).errors)
          @$(".product-summary.error").removeClass("hidden")

  imageUploaded: (item) =>
    importResultView = new Admin.Views.ImportImageResultView(model: item)
    @importResultViews.push importResultView
    @appendChildTo importResultView, ".import-results"

  highlight: () =>
    #no-op
  getRow: () =>
    #no-op
  getModel: () =>
    #no-op
  scroll: () =>

  title: () =>
    "Import Details"

  addSpinner: (element) =>
    spinner = new Spinner(
      lines: 13, length: 2, width: 2, radius: 3, corners: 1, rotate: 0,
      color: '#fff', speed: 1, trail: 60, shadow: false).spin()
    $(spinner.el).css("display", "inline-block").css("margin-bottom", "5px").css('margin-left', '10px').css('margin-right', '5px')
    element.append($('<i>').addClass('spin-wrapper icon-white').append(spinner.el))
