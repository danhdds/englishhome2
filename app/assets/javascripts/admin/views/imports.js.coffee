#= require './properties'
class Admin.Views.ImportView extends Admin.Views.PropertiesView
  template: JST['admin/templates/import']

  events:
    "click .close": "close"

  className: 'popover right product'

  renderDetails: () =>
    super


  getRow: () =>
    $("#clickable_for_attribute_#{@model.id}")

  title: () =>
    "Import Details"