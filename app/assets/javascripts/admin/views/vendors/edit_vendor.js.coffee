class Admin.Views.EditVendorView extends Admin.Views.PropertiesView
  template: JST['admin/templates/edit_vendor']

  events:
    "click .close": "close"
    "click .btn-submit" : "createVendor"

  className: 'popover right user'

  render: () =>
    @$el.html @template()
    @$(".title").text(@title())

  renderDetails: () =>
    super
    @$(".name").val(@model.get("name"))
    @$(".position").val(@model.get("position"))
    @$(".map-url").val(@model.get("map_url")) if @model.get("map_url")
    @$(".address").text(@model.get("address"))
    @$(".map-img").val(@model.get("google_map_img"))
    @$('.show input').prop('checked', true) if @model.get("show")
    @$('.head-office input').prop('checked', true) if @model.get('head_office')
    super

  getRow: () =>
    $("#clickable_for_vendor_#{@model.id}")

  title: () =>
    "Edit Location"

  createVendor: () =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    @model.save
      name: @$(".name").val()
      position: @$(".position").val()
      address: @$(".address").val()
      map_url: @$(".map-url").val()
      google_map_img: @$(".map-img").val()
      show: @$('.show input').prop('checked')
      head_office: @$('.head-office input').prop('checked')
    , success: () =>
        window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")
