#= require '../properties'
class Admin.Views.VendorView extends Admin.Views.PropertiesView
  template: JST['admin/templates/vendor']

  events:
    "click .close": "close"

  className: 'popover right user'

  renderDetails: () =>
    super
    @$(".name").text(@model.get("name"))
    @$(".map-url").text(@model.get("map_url")) if @model.get("map_url")
    @$(".address").html(@model.get("address").replace(/\r\n|\r|\n/g,"<br />"))
    @$(".map-img").text(@model.get("google_map_img"))
    @$('.show input').prop('checked', true) if @model.get("show")
    @$('.head-office input').prop('checked', true) if @model.get('head_office')
    super

  getRow: () =>
    $("#clickable_for_vendor_#{@model.id}")

  title: () =>
    "Location Details"