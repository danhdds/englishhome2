class Admin.Views.AddVendorView extends Admin.Views.PropertiesView
  template: JST['admin/templates/add_vendor']

  events:
    "click .close": "close"
    "click .btn-submit" : "createVendor"

  className: 'popover bottom add-user'

  render: () =>
    @$el.html @template()
    @$(".title").text(@title())

  highlight: () =>
    #no-op
  getRow: () =>
    #no-op
  getModel: () =>
    #no-op
  scroll: () =>

  title: () =>
    "Create Location"

  createVendor: () =>
    $.ajaxSetup headers:
      "X-CSRF-Token": $("meta[name=\"csrf-token\"]").attr("content")

    vendor = new Admin.Models.Vendor
      name: @$(".name").val()
      position: @$(".position").val()
      address: @$(".address").val()
      map_url: @$(".map-url").val()
      google_map_img: @$(".map-img").val()
      show: @$('.show input').prop('checked')
      head_office: @$('.head-office input').prop('checked')
    vendor.save {},
      success: () =>
        window.location.reload()
      error: (data, response) =>
        @$(".errors").removeClass('hidden')
        @$(".errors").html("")
        _.each JSON.parse(response.responseText).errors, (error, key) =>
          @$(".errors").append("<div>#{key} #{error}</div>")
