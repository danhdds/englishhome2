class Admin.Collections.GalleryImages extends Backbone.Collection
  model: Admin.Models.GalleryImage
  url: "/admin/gallery_images"