window.Admin =
  Models: {}
  Collections: {}
  Routers: {}
  Views:
    Texts: {}
  Bootstrap: {}
  Data: {}

  init: (options)->
    $ ->
      Admin.app = new Admin.Routers.Main(options)
      Backbone.history.start()
      Admin.pushState = history.pushState?

      $(".clickable").on "click", (e) =>
        url = $(e.currentTarget).attr("fragment")
        title = $(e.currentTarget).attr("title")
        Admin.app.navigate url,
          trigger: true

      $(".select2able").select2()
      $(".filters input.submit").hide()
      $(".filters select").on "change", () ->

        $(@).parents("form").submit()
      $("th.sortable").on "click", () ->


        console.log $(@).attr("sort-url")
        url = $(@).attr("sort-url")


        window.location = url

