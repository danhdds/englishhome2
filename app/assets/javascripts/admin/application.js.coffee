#
# Main
#
#= require jquery
#= require underscore
#= require backbone
#= require backbone-support
#= require ./lib/backbone_overrides
#= require hamlcoffee
#= require twitter/bootstrap/transition
#= require twitter/bootstrap/dropdown
#= require twitter/bootstrap/tooltip
#= require twitter/bootstrap/modal
#= require twitter/bootstrap/popover
#= require twitter/bootstrap/tab
#= require select2
#
# Libs
#
#= require plugins/jquery.validate
#= require jquery.ui.sortable
#= require plugins/backbone.syphon
#= require plugins/backbone-safesync
#= require plugins/pusher.min
#= require plugins/backbone.bootstrap-modal
#= require plugins/moment
#= require plugins/jquery.ui.widget
#= require plugins/jquery.iframe-transport
#= require plugins/jquery.fileupload
#= require plugins/backgrid
#= require plugins/spin.min


#
# App
#
#= require ./main
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers
