class Admin.Models.Attribute extends Backbone.Model

  urlRoot: "/admin/attributes"
  name: "attribute"

  createdAt: () =>
    moment(@get("created_at ")).format('DD/MM/YYYY HH:MM:SS')
