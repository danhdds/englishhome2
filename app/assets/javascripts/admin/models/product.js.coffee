class Admin.Models.Product extends Backbone.Model

  urlRoot: "/admin/products"
  name: "product"

  createdAt: () =>
    moment(@get("created_at ")).format('DD/MM/YYYY HH:MM:SS')

  parse: (response) =>
    @audits = new Backbone.Collection(response.audits)
    response