class Admin.Models.User extends Backbone.Model

  urlRoot: "/admin/users"
  name: "users"

  createdAt: () =>
    moment(@get("created_at")).format('DD/MM/YYYY HH:MM:SS')

  lastLoginAt: () =>
    if @get("last_login_at")?
      moment(@get("last_login_at")).format('DD/MM/YYYY HH:MM:SS')
    else
      "Never"

  wholeName: () =>
    name = ""
    name += @get("first_name") if @get("first_name")?
    name += " "
    name += @get("last_name") if @get("last_name")?
    name