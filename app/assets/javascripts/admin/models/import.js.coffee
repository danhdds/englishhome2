class Admin.Models.Import extends Backbone.Model

  urlRoot: "/admin/imports"
  name: "import"

  createdAt: () =>
    moment(@get("created_at ")).format('DD/MM/YYYY HH:MM:SS')

  parse: (response) =>
    @product = new Admin.Models.Product(response.product) if response.product?
    response