class Admin.Models.GalleryImage extends Backbone.Model
  modelName: "gallery_image"
  urlRoot: "/admin/gallery_images"
  name: "gallery_image"



  createdAt: () =>
    moment(@get("created_at ")).format('DD/MM/YYYY HH:MM:SS')

  # parse: (response, options) =>
  #   console.log "PARSING:::"
  #   console.log response
  #   response.attributes

