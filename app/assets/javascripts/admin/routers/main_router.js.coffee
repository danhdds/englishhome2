class Admin.Routers.Main extends Backbone.Router

  routes:
    "products/add": "addProducts"
    "products/:id": "showProduct"
    "attributes/:id": "showAttribute"
    "designs/:id": "showDesign"
    "gallery_images/add": "addGalleryImage"
    "gallery_images/:id": "showGalleryImage"
    "users/add": "addUser"
    "users/:id": "showUser"
    "users/:id/edit": "editUser"
    "vendors/add": "addVendor"
    "vendors/:id": "showVendor"
    "vendors/:id/edit": "editVendor"
    "site_texts/add": "addSiteText"
    "site_texts/:id": "showSiteText"
    "site_texts/:id/edit": "editSiteText"
    "menu_images/add"      : "addMenuImage"
    "menu_images/:id"      : "showMenuImage"
    "menu_images/:id/edit" : "editMenuImage"

  initialize: (options) =>

  showUser: (id) =>
    @hidePanels()
    @user = new Admin.Models.User
      id: id

    @view = new Admin.Views.UserView(model: @user)
    @view.show()

  showGalleryImage: (id) =>
    @hidePanels()
    @galleryImage = new Admin.Models.GalleryImage
      id: id

    @view = new Admin.Views.GalleryImageView(model: @galleryImage)
    @view.show()

  addGalleryImage: () =>
    @hidePanels()
    @view = new Admin.Views.AddGalleryImageView()
    @view.show()

  showDesign: (id) =>
    @hidePanels()
    @design = new Admin.Models.Design
      id: id

    @view = new Admin.Views.DesignView(model: @design)
    @view.show()

  showProduct: (id) =>
    @hidePanels()
    @product = new Admin.Models.Product
      id: id

    @view = new Admin.Views.ProductView(model: @product)
    @view.show()

  showAttribute: (id) =>
    @hidePanels()
    @attribute = new Admin.Models.Attribute
      id: id

    @view = new Admin.Views.AttributeView(model: @attribute)
    @view.show()

  addProducts: (id) =>
    @hidePanels()
    @view = new Admin.Views.AddProductsView()
    console.log @view
    @view.show()

  addUser: (id) =>
    @hidePanels()
    @view = new Admin.Views.AddUserView()
    @view.show()

  editUser: (id) =>
    @hidePanels()
    @user = new Admin.Models.User
      id: id

    @view = new Admin.Views.EditUserView(model: @user)
    @view.show()

  addVendor: () =>
    @hidePanels()
    @view = new Admin.Views.AddVendorView()
    @view.show()

  showVendor: (id) =>
    @hidePanels()
    @vendor = new Admin.Models.Vendor
      id: id

    @view = new Admin.Views.VendorView(model: @vendor)
    @view.show()

  editVendor: (id) =>
    @hidePanels()
    @vendor = new Admin.Models.Vendor
      id: id

    @view = new Admin.Views.EditVendorView(model: @vendor)
    @view.show()

  addSiteText: () =>
    @hidePanels()
    @view = new Admin.Views.AddSiteTextView()
    @view.show()

  showSiteText: (id) =>
    @hidePanels()
    @site_text = new Admin.Models.SiteText
      id: id

    @view = new Admin.Views.SiteTextView(model: @site_text)
    @view.show()

  editSiteText: (id) =>
    @hidePanels()
    @site_text = new Admin.Models.SiteText
      id: id

    @view = new Admin.Views.EditSiteTextView(model: @site_text)
    @view.show()

  addMenuImage: =>
    @hidePanels()
    @view = new Admin.Views.AddMenuImageView()
    @view.show()

  showMenuImage: (id) =>
    @hidePanels()
    @menu_image = new Admin.Models.MenuImage(
      id: id)

    @view = new Admin.Views.MenuImageView(model: @menu_image)
    @view.show()

  hidePanels: () =>
    @view?.hide()
