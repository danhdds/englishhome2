# Override Backbone.sync to use the PUT HTTP method for PATCH requests
# when doing Model#save({...}, { patch: true });

originalSync = Backbone.sync
Backbone.sync = (method, model, options) ->
  console.log method, model, options
  if (method == 'patch')
    options.type = 'PUT'
  originalSync(method, model, options)


