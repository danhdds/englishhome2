$(function() {
  width = $("#gallery .slides").width();
  max_width = $("#gallery ol").width();
  offset = Math.floor(width / $("li.thumb").width()) * $("li.thumb").width();

  $("#gallery .thumb").click(function() {
    slide = $(this).attr("slide")
    $("#gallery .slide").hide();
    img = $("#gallery").find("#"+slide +" img");
    img.attr("src", img.attr("href"));

    next_img = $("#gallery").find("#"+slide).next(".slide").find("img");
    next_img.attr("src", next_img.attr("href"));

    $("#gallery").find("#"+slide).show();

    $("#gallery .thumb").removeClass("current");
    $(this).addClass("current")




    left = $(this).position().left;
    right = left+$(this).width();

    if (right > width) {

      var margin_left = parseInt($("#gallery ol").css("margin-left"));
      $("#gallery ol").animate({"margin-left":margin_left-offset+"px"})
    }
  });

  $("#gallery .left").click(function() {
    var margin_left = parseInt($("#gallery ol").css("margin-left"));
    if (margin_left < 0) {
      $("#gallery ol").animate({"margin-left":margin_left+offset+"px"});
    }
  });

  $("#gallery .right").click(function() {
    var margin_left = parseInt($("#gallery ol").css("margin-left")) - offset;

    if (margin_left > -max_width) {
      $("#gallery ol").animate({"margin-left":margin_left+"px"});
    }
  });

})

