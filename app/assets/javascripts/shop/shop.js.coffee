$ ->
  $("li.with-submenu").on "mouseenter", ->
    clearTimeout(window.menuTimer) if window.menuTimer?
    window.menuTimer = null
    $("#screen").show()
    $("#screen").removeClass("hidden")
    $("#screen").addClass("visible")

  $("li.with-submenu").on "mouseleave", ->
    window.menuTimer = setTimeout ->
      $("#screen").removeClass("visible")
      $("#screen").addClass("hidden")
    , 1

  $("#screen").on "webkitAnimationEnd oAnimationEnd
    animationend msAnimationEnd", ->
    console.log("hide")
    $("#screen").hide() if window.menuTimer?
    window.menuTimer = null

  $(".sub-menu.products .left").on "mouseenter", ->
    $(".sub-menu.products .contemporary").removeClass("hidden")
    $(".sub-menu.products .classic").addClass("hidden")

  $(".sub-menu.products .right").on "mouseenter", ->
    $(".sub-menu.products .classic").removeClass("hidden")
    $(".sub-menu.products .contemporary").addClass("hidden")

  $(".product .main .zoomable").on "click", ->
    height = parseInt($("#container").css("height")) - 150
    $(".product .zoom").css("height", height+"px")
    $(".product .zoom").removeClass('hidden')

  $(".product .zoom").on "click", ->
    $(".product .zoom").addClass("hidden")

  $("li.with-submenu").on "touchstart", (e) ->
    # e.preventDefault()
    e.stopPropagation()
    $(this).toggleClass("hover_effect")
    $("#screen").toggleClass("visible")
    $("#screen").toggleClass("hidden")
