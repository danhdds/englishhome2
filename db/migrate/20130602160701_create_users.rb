class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string   :email
      t.string   :crypted_password
      t.string   :password_salt
      t.string   :persistence_token
      t.string   :first_name
      t.string   :last_name
      t.datetime :last_request_at
      t.integer  :login_count
      t.datetime :current_login_at
      t.datetime :last_login_at
      t.boolean  :admin
      t.text     :bio
      t.integer  :position
      t.string   :mugshot_file_name
      t.timestamps
    end
  end
end
