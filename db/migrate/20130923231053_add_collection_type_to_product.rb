class AddCollectionTypeToProduct < ActiveRecord::Migration
  def change
    add_column :products, :collection_type, :string
    add_index :products, [:collection_type, :product_code_id]
  end
end
