class CreateOthers < ActiveRecord::Migration
  def change
    create_table :others do |t|
      t.string :name
      t.string :code

      t.timestamps
    end
  end
end
