class CreateMenuImages < ActiveRecord::Migration
  def change
    create_table :menu_images do |t|
      t.boolean :products_classic, :defualt => false
      t.boolean :products_contemporary, :defualt => false
      t.boolean :collections_classic, :defualt => false
      t.boolean :collections_contemporary, :defualt => false
      t.boolean :enabled, :defualt => false

      t.timestamps
    end
  end
end
