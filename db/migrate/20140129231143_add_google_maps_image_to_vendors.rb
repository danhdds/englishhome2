class AddGoogleMapsImageToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :google_map_img, :text
  end
end
