class AddAttachmentToMenuImages < ActiveRecord::Migration
  def change
    add_attachment :menu_images, :photo
  end
end
