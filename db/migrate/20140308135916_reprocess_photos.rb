class ReprocessPhotos < ActiveRecord::Migration
  def up
    Design.has_photo.each do |d|
      d.photo.reprocess! if d.photo.exists?
    end
  end

  def down
  end
end
