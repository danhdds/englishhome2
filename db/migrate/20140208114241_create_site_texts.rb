class CreateSiteTexts < ActiveRecord::Migration
  def change
    create_table :site_texts do |t|
      t.text :text
      t.boolean :about_us
      t.boolean :trade
      t.boolean :enabled, :default => false

      t.timestamps
    end
  end
end
