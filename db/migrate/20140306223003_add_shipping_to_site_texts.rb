class AddShippingToSiteTexts < ActiveRecord::Migration
  def change
    add_column :site_texts, :shipping, :boolean, :default => false
  end
end
