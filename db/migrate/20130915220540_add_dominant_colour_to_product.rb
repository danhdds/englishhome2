class AddDominantColourToProduct < ActiveRecord::Migration
  def change
    add_column :products, :dominant_colour, :string
  end
end
