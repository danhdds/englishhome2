class CreateCategoryProductTypes < ActiveRecord::Migration
  def change
    create_table :category_product_types do |t|
      t.references :category
      t.references :product_type

      t.timestamps
    end
    add_index :category_product_types, :category_id
    add_index :category_product_types, :product_type_id
  end
end
