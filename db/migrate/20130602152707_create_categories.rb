class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.datetime :deleted_at
      t.string   :name
      t.timestamps
    end
  end
end
