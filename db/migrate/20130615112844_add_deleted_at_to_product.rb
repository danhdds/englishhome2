class AddDeletedAtToProduct < ActiveRecord::Migration
  def change
    add_column :products, :deleted_at, :datetime
    add_column :products, :deleted_by_id, :integer
  end
end
