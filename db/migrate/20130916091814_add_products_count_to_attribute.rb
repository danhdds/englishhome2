class AddProductsCountToAttribute < ActiveRecord::Migration
  def change
    add_column :attributes, :products_count, :integer, :default => 0
  end
end
