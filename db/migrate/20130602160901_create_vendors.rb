class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :name
      t.text :address
      t.text :map_url
      t.integer :position

      t.timestamps
    end
  end
end
