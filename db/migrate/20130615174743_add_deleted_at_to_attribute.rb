class AddDeletedAtToAttribute < ActiveRecord::Migration
  def change
    add_column :attributes, :deleted_at, :datetime
    add_column :attributes, :deleted_by_id, :integer
  end
end
