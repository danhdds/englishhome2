class AddColumnsToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :head_office, :boolean, :default => false
    add_column :vendors, :show, :boolean, :default => false
  end
end
