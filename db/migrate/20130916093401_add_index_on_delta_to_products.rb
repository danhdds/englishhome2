class AddIndexOnDeltaToProducts < ActiveRecord::Migration
  def change
    add_index  :products, :delta
  end
end
