class ChangeCollectionTypeInProducts < ActiveRecord::Migration
  def up
    change_column :products, :collection_type, :boolean
  end
end
