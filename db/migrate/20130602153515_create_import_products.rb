class CreateImportProducts < ActiveRecord::Migration
  def change
    create_table :import_products do |t|
      t.integer :product_id
      t.boolean :updated

      t.timestamps
    end
  end
end
