class AddCollectionTypeToAttributes < ActiveRecord::Migration
  def change
    add_column :attributes, :collection_type, :string
  end
end
