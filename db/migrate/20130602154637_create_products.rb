class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string   :code
      t.integer  :base_colour_id
      t.integer  :base_fabric_id
      t.integer  :design_id
      t.integer  :product_code_id
      t.integer  :shape_id
      t.integer  :applique1_id
      t.integer  :applique2_id
      t.integer  :applique3_id
      t.string   :name
      t.integer  :with_option_id
      t.integer  :and_option_id
      t.integer  :dimension_id
      t.integer  :style_id
      t.string   :photo_file_name
      t.string   :photo_content_type
      t.integer  :photo_file_size
      t.integer  :other1_id
      t.integer  :other2_id
      t.decimal  :price,              :precision => 8, :scale => 2
      t.boolean  :delta,                                            :default => true, :null => false
      t.integer  :revision,                                         :default => 0,    :null => false
      t.text     :photo_styles
      t.string   :short_code
      t.integer  :position,                                        :default => 0

      t.timestamps
    end
  end
end
