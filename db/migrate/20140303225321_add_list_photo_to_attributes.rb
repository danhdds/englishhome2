class AddListPhotoToAttributes < ActiveRecord::Migration
  def change
    add_attachment :attributes, :list_photo
  end
end
