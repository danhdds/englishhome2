class AddAccentIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :accent_id, :integer
  end
end
