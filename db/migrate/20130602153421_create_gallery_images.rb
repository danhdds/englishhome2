class CreateGalleryImages < ActiveRecord::Migration
  def change
    create_table :gallery_images do |t|
      t.integer :postiion
      t.string :image_file_name
      t.references :collection1
      t.references :collection2
      t.references :collection3
      t.boolean :home_page

      t.timestamps
    end
    add_index :gallery_images, :collection1_id
    add_index :gallery_images, :collection2_id
    add_index :gallery_images, :collection3_id
  end
end
