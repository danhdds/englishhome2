class CreateAttributes < ActiveRecord::Migration
  def change
    create_table :attributes do |t|
      t.string   :code
      t.string   :name
      t.string   :type
      t.string   :colour
      t.string   :detail
      t.string   :metric_description
      t.string   :imperial_description
      t.string   :description
      t.string   :dimension
      t.integer  :column_count
      t.string   :hide_from_countries,   :default => ""
      t.integer  :design_position
      t.integer  :product_code_position
      t.string   :plural
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
