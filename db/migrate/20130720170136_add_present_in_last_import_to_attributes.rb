class AddPresentInLastImportToAttributes < ActiveRecord::Migration
  def change
    add_column :attributes, :present_in_last_import, :boolean
  end
end
