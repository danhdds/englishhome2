class AddPhotoToAttributes < ActiveRecord::Migration
  def change
    add_attachment :attributes, :photo
  end
end
