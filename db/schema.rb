# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140308142537) do

  create_table "attributes", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "colour"
    t.string   "detail"
    t.string   "metric_description"
    t.string   "imperial_description"
    t.string   "description"
    t.string   "dimension"
    t.integer  "column_count"
    t.string   "hide_from_countries",     :default => ""
    t.integer  "design_position"
    t.integer  "product_code_position"
    t.string   "plural"
    t.datetime "deleted_at"
    t.integer  "deleted_by_id"
    t.boolean  "present_in_last_import"
    t.string   "collection_type"
    t.integer  "products_count",          :default => 0
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "position"
    t.string   "list_photo_file_name"
    t.string   "list_photo_content_type"
    t.integer  "list_photo_file_size"
    t.datetime "list_photo_updated_at"
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_product_types", :force => true do |t|
    t.integer  "category_id"
    t.integer  "product_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.boolean  "charge_vat", :default => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "gallery_images", :force => true do |t|
    t.integer  "position"
    t.string   "image_file_name"
    t.integer  "collection1_id"
    t.integer  "collection2_id"
    t.integer  "collection3_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "home_page",       :default => false
  end

  create_table "import_products", :force => true do |t|
    t.integer  "import_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "updated"
  end

  create_table "imports", :force => true do |t|
    t.string   "upload_file_name"
    t.string   "upload_content_type"
    t.integer  "upload_file_size"
    t.datetime "upload_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "import_type"
    t.integer  "user_id"
  end

  create_table "menu_images", :force => true do |t|
    t.boolean  "products_classic"
    t.boolean  "products_contemporary"
    t.boolean  "collections_classic"
    t.boolean  "collections_contemporary"
    t.boolean  "enabled"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "others", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", :force => true do |t|
    t.string   "code"
    t.integer  "base_colour_id"
    t.integer  "base_fabric_id"
    t.integer  "design_id"
    t.integer  "product_code_id"
    t.integer  "shape_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "applique1_id"
    t.integer  "applique2_id"
    t.integer  "applique3_id"
    t.string   "name"
    t.integer  "with_option_id"
    t.integer  "and_option_id"
    t.integer  "dimension_id"
    t.integer  "style_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.integer  "other1_id"
    t.integer  "other2_id"
    t.decimal  "price",              :precision => 8, :scale => 2
    t.boolean  "delta",                                            :default => true, :null => false
    t.integer  "revision",                                         :default => 0,    :null => false
    t.text     "photo_styles"
    t.string   "short_code"
    t.integer  "position",                                         :default => 0
    t.boolean  "enabled"
    t.datetime "deleted_at"
    t.integer  "deleted_by_id"
    t.integer  "accent_id"
    t.string   "dominant_colour"
    t.boolean  "collection_type"
  end

  add_index "products", ["collection_type", "product_code_id"], :name => "index_products_on_collection_type_and_product_code_id"
  add_index "products", ["delta"], :name => "index_products_on_delta"

  create_table "site_texts", :force => true do |t|
    t.text     "text"
    t.boolean  "about_us"
    t.boolean  "trade"
    t.boolean  "enabled",    :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "shipping",   :default => false
  end

  create_table "styles", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_request_at"
    t.integer  "login_count"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.boolean  "admin"
    t.text     "bio"
    t.integer  "position"
    t.string   "mugshot_file_name"
  end

  create_table "vendors", :force => true do |t|
    t.string   "name"
    t.text     "address"
    t.text     "map_url"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "head_office",    :default => false
    t.boolean  "show",           :default => false
    t.text     "google_map_img"
  end

end
