Dir.glob(File.join(Rails.root,'app','models','**','*.rb')).each do |file|
  require_dependency file
end

ATTRIBUTE_TYPES = Attribute.send(:subclasses).map {|type| type.to_s }