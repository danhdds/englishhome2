class Time
  def humanize(include_year=false)
    current = Date.today
    case
    when self.year != current.year
      strftime("%e %B %Y")
    when self.yday == (current.yday + 1)
      "Tomorrow"
    when self.yday == current.yday
      self.to_formatted_s(:hhmm)
    when self.yday == (current.yday - 1)
      "Yesterday"
    else
      if include_year
        strftime("%e %B %Y")
      else
        strftime("%e %B")
      end

    end
  end
end
Date::DATE_FORMATS[:filename_ddmmyyyy] = "%d_%m_%Y"
Date::DATE_FORMATS[:mmyyyy] = "%b %y"
Date::DATE_FORMATS[:ddmmyyyy] = "%d/%m/%Y"

Time::DATE_FORMATS[:ddmmyyyy] = "%d/%m/%Y"
Time::DATE_FORMATS[:hhmm] = "%H:%M"

Time::DATE_FORMATS[:short_and_year] = "%d/%m/%y %H:%M"
