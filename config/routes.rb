Englishhome2::Application.routes.draw do

  root :to => "shop#index", :as => "shop"

  resources :products do
    collection do
      get 'search'
    end
  end

  resources :categories do
  end

  resources :collections do
    collection do
      get 'classic'
      get 'contemporary'
    end
  end

  match 'info/contact_us' => 'shop#contact_us'
  match 'info/about_us' => 'shop#about_us'
  match 'info/shipping' => 'shop#shipping'
  # match 'info/media' => 'shop#media'
  match 'info/trade' => 'shop#trade'

    namespace :admin do

      root :to => "products#index", :as => "root"

      resources :products do
        member do
          post 'upload_photo'
        end
      end
      resources :attributes
      resources :gallery_images do
        member do
          post 'upload'
        end
      end
      resources :designs do
        member do
          post 'moodshot'
          post 'list_photo'
        end
      end
      resources :users
      resources :imports do
        collection do
          post 'import_price_list'
          post 'import_image'
        end
      end

      resources :vendors
      resources :site_texts

      resources :image_imports
      resources :price_list_imports
      resources :menu_images do
        member do
          post 'upload_photo'
        end
      end

      resource :user_session
      match 'login' => "user_sessions#new",      :as => :login
      match 'logout' => "user_sessions#destroy", :as => :logout
    end

  get "/500", :to => "errors#server_error"
  get "/404", :to => "errors#not_found"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
