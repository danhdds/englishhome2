# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'englishhome2'
set :repo_url, 'git@github.com:superslau/englishhome2.git'
set :branch, "rich-changes"

set :user, 'deploy'
set :use_sudo, false

# Only have production server
set :rails_env, "production"

set :deploy_via, :copy

set :stages, %w[staging production]
set :default_stage, 'staging'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/srv/englishhome2'
set :bundle_without, [:test, :cucumber]

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/production.sphinx.conf}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
after 'deploy:publishing', 'deploy:restart'

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
      within release_path do
        with rails_env: fetch(:rails_env)do
          execute :rake, 'ts:generate'
          execute :rake, 'ts:index'
          execute :rake, 'ts:stop'
          execute :rake, 'ts:start'
        end
      end
    end
    invoke 'delayed_job:restart'
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
