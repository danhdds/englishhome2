namespace :products do
  task :update_type => :environment do
    Product.find_in_batches do |batch|
      batch.each do |product|
        product.set_collection_type
        product.save
      end
    end
  end
end